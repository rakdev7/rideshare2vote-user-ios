//
//  ViewTransactionModel.swift
//
//  Created by Atul Jain on 29/03/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public class ViewTransactionModel {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let details = "details"
    static let status = "status"
    static let message = "message"
  }

  // MARK: Properties
  public var details: [ViewTransactionDetails]?
  public var status: Int?
  public var message: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    if let items = json[SerializationKeys.details].array { details = items.map { ViewTransactionDetails(json: $0) } }
    status = json[SerializationKeys.status].int
    message = json[SerializationKeys.message].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = details { dictionary[SerializationKeys.details] = value.map { $0.dictionaryRepresentation() } }
    if let value = status { dictionary[SerializationKeys.status] = value }
    if let value = message { dictionary[SerializationKeys.message] = value }
    return dictionary
  }

}
