//
//  BaseClassModel.swift
//
//  Created by Atul Jain on 13/06/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON
public class BaseClassModel {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let message = "message"
    static let result = "result"
    static let response = "response"
  }

  // MARK: Properties
  public var message: String?
  public var result: Int?
  public var response: [Response]?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    message = json[SerializationKeys.message].string
    result = json[SerializationKeys.result].int
    if let items = json[SerializationKeys.response].array { response = items.map { Response(json: $0) } }
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = message { dictionary[SerializationKeys.message] = value }
    if let value = result { dictionary[SerializationKeys.result] = value }
    if let value = response { dictionary[SerializationKeys.response] = value.map { $0.dictionaryRepresentation() } }
    return dictionary
  }

}
