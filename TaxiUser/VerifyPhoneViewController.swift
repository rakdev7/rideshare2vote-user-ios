//
//  VerifyPhoneViewController.swift
//  YoTaxiCab Customer
//
//  Created by AppOrio on 04/10/17.
//  Copyright © 2017 apporio. All rights reserved.
//

import UIKit

class VerifyPhoneViewController: UIViewController,MainCategoryProtocol,MICountryPickerDelegate {
    
    
    //@IBOutlet var container: UIView!
    
    var checkotpdata : CheckOtpModel!
    
    var forgotcheckotpdata : ForgotCheckOtpModel!
    
    
    @IBOutlet weak var countrycodetext: UILabel!
    
    @IBOutlet weak var enterphonetext: UITextField!
    
    @IBOutlet weak var enterotptext: UITextField!
    
    var otpvalue = "0"
    
    @IBOutlet weak var pleaseenterdetailstext: UILabel!
    @IBOutlet weak var getotpbtntext: UIButton!
    @IBOutlet weak var verifyphonenumbertextlabel: UILabel!
   
    @IBOutlet weak var submitbtntext: UIButton!
   var matchString = ""
    
    var selcetcountrycode = "+91"
    
    var phonetext = ""

    @IBOutlet weak var otpcontainer: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        setColor()
        enterphonetext.keyboardType = .emailAddress
        pleaseenterdetailstext.text = "Please enter the One Time Password(OTP) received on your entered Email address!".localized
        verifyphonenumbertextlabel.text = "Verify Email Address".localized
        getotpbtntext.setTitle("GET OTP".localized, for: UIControlState.normal)
        submitbtntext.setTitle("Submit".localized, for: UIControlState.normal)
        enterphonetext.placeholder = "Enter Email".localized
        enterotptext.placeholder = "Enter Otp".localized
        
        
        
      //   self.container.edgeWithShadow()
        
       // self.otpcontainer.edgeWithShadow()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backbtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func setColor() {
        submitbtntext.backgroundColor = AppColors.themeColor
        submitbtntext.setTitleColor(AppColors.whiteColor, for: .normal)
        getotpbtntext.backgroundColor = AppColors.themeColor
        getotpbtntext.setTitleColor(AppColors.whiteColor, for: .normal
        )
    }

    
    func countryPicker(_ picker: MICountryPicker, didSelectCountryWithName name: String, code: String) {
        
    }
    func countryPicker(_ picker: MICountryPicker, didSelectCountryWithName name: String, code: String, dialCode: String)
    {
        selcetcountrycode = dialCode
        countrycodetext.text = dialCode
        self.dismiss(animated: true, completion: nil)
        
        
    }
    @IBAction func Selectcountrycode_btn(_ sender: Any) {
        
        let picker = MICountryPicker { (name, code) -> () in
            print(code)
        }
        
        picker.delegate = self
        let backButton = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonTapped))
        picker.navigationItem.leftBarButtonItem = backButton
        // Display calling codes
        picker.showCallingCodes = true
        
        // or closure
        picker.didSelectCountryClosure = { name, code in
        }
        
       // self.present(picker, animated: true, completion: nil)
        let navcontroller = UINavigationController(rootViewController: picker)
        
        self.present(navcontroller,animated: true,completion: nil)

    }
    
    func backButtonTapped() {
        
        self.dismiss(animated: true, completion: nil)
    }

    
    @IBAction func getotp_btn_click(_ sender: Any) {
        
        phonetext = self.enterphonetext.text!
        
        if (!phonetext.contains("@"))
        {
            self.showalert(message: "Wrong Email Format".localized)

        }
            
        else if (phonetext.contains(" "))
        {
            self.showalert(message: "Email id must not contain space".localized)

        }
            
        else {

            enterotptext.becomeFirstResponder()
        
        if self.matchString == "forgot"{
        
            ApiManager.sharedInstance.protocolmain_Catagory = self
            ApiManager.sharedInstance.ForgotGetOtpMethod(email: self.enterphonetext.text!)
        
        }else{
        
        ApiManager.sharedInstance.protocolmain_Catagory = self
        ApiManager.sharedInstance.GetOtpMethod(Phone: selcetcountrycode + self.enterphonetext.text!)
            
        }
            
        }
        
    }
   
    
    @IBAction func Submit_btn_click(_ sender: Any) {
        
        phonetext = self.enterphonetext.text!
        
        if phonetext == ""{
            
            self.showalert(message: "Please Enter Email Address First".localized)
           
           
        }else{
        
        if otpvalue == self.enterotptext.text!
            
        {
            
             if self.matchString == "forgot"{
                
//                 GlobalVarible.enteruserphonenumber = selcetcountrycode + self.enterphonetext.text!
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let vc = storyBoard.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
                vc.emailId = phonetext
                self.present(vc, animated: true, completion: nil)

             }else{
                GlobalVarible.checkphonenumber = 1
                GlobalVarible.enteruserphonenumber = selcetcountrycode + self.enterphonetext.text!
               self.dismiss(animated: true, completion: nil)
            
              }
            
            
        }else{
            
             self.showalert(message: "Please Enter Valid OTP".localized)
        }
        
        
        
        }
        
        
        
    }
    
    
    func showalert(message:String)  {
        
        DispatchQueue.main.async(execute: {
            
            let alertController = UIAlertController(title:   "Alert".localized, message:message, preferredStyle: .alert)
            
            
            let OKAction = UIAlertAction(title: "ok".localized, style: .default) { (action) in
                
            }
            alertController.addAction(OKAction)
            
            self.present(alertController, animated: true) {
                
            }
            
            
        })
        
    }
    


    
    func onProgressStatus(value: Int) {
        if(value == 0 ){
            MBProgressHUD.hide(for: self.view, animated: true)
        }else if (value == 1){
            let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
            spinnerActivity.label.text = "Loading".localized
            spinnerActivity.detailsLabel.text = "Please Wait!!".localized
            spinnerActivity.isUserInteractionEnabled = false
            
        }
    }
    
    func onSuccessExecution(msg: String) {
        print("\(msg)")
    }
    
    
    func onerror(msg : String) {
        MBProgressHUD.hide(for: self.view, animated: true)
        
        self.showalert(message: msg)
        
        
    }
    
    
    func onSuccessParse(data: AnyObject) {
        
        if(GlobalVarible.Api == "CheckOtpModel"){
        
            if let checkotpdata = data as? CheckOtpModel{
            
                self.checkotpdata = checkotpdata
            if(checkotpdata.result == 1){
                
            self.otpvalue = checkotpdata.otp!
                
               // self.enterotptext.text! = checkotpdata.otp!
                
            }else{
                
            self.showalert(message: checkotpdata.msg!)
            }
        
            }
        }
        
        if(GlobalVarible.Api == "ForgotCheckOtpModel"){
            
            if let forgotcheckotpdata = data as? ForgotCheckOtpModel{
            self.forgotcheckotpdata = forgotcheckotpdata
            if(forgotcheckotpdata.result == 1){
                
                self.otpvalue = forgotcheckotpdata.otp!
                
               // self.enterotptext.text! = forgotcheckotpdata.otp!
                
            }else{
                
                self.showalert(message: forgotcheckotpdata.msg!)
            }
            
            }
        }
        
        
        
        
    }
    

   
}
