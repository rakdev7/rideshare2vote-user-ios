//
//  SignupViewControllerWithFacebookGoogle.swift
//  TaxiUser
//
//  Created by AppOrio on 22/05/17.
//  Copyright © 2017 apporio. All rights reserved.
//

import UIKit


class SignupViewControllerWithFacebookGoogle: UIViewController,MainCategoryProtocol {
    
    var logindata : SignupLoginResponse!
    
    @IBOutlet weak var userimageview: UIImageView!
    
    @IBOutlet weak var username: UILabel!
    
    @IBOutlet weak var useremail: UILabel!
    
    
    @IBOutlet var container: UIView!
    
//    @IBOutlet var phoneButton: UIButton!
    
  //  @IBOutlet weak var countrycodetext: UILabel!
    
   // @IBOutlet weak var enterphonetext: UITextField!
    
    @IBOutlet weak var registerbtntext: UIButton!
    
    @IBOutlet weak var topregistertextlabel: UILabel!
    
    @IBOutlet weak var attachdetailstextlabel: UILabel!
    
   // @IBOutlet var firstName: UITextField!
   // @IBOutlet var lastName: UITextField!
    
   // var selcetcountrycode = "+91"
    @IBOutlet weak var countrycodetext: UILabel!
    @IBOutlet weak var enterphonetext: UITextField!
    var selcetcountrycode = "+1"

    var phonetext = ""
    
    var movedFrom = ""
    
    var facebookFirstName = ""
    var facebookLastName = ""
    var googleFirstName = ""
    var googleLastName = ""
    var facebookId = ""
    var googleId = ""
    var googleMail = ""
    var googleImage = ""
    var facebookMail = ""
    var facebookImage = ""
    
    func setupView(){
        
        attachdetailstextlabel.text = "Attach Your Phone number if you want to remember your account easily.".localized
        
        topregistertextlabel.text = "Register".localized
        //phoneButton.setTitle("Enter Phone".localized, for: UIControlState.normal)
        //enterphonetext.placeholder = "Enter Phone".localized
        registerbtntext.setTitle("Register".localized, for: UIControlState.normal)
        
    }



    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.setupView()
        
        userimageview.layer.cornerRadius =  userimageview.frame.width/2
        userimageview.clipsToBounds = true
        userimageview.layer.borderWidth = 1
        userimageview.layer.borderColor = UIColor.white.cgColor
        


        self.container.edgeWithShadow()
        
        if movedFrom == "google"{
            
            if(googleImage == ""){
                userimageview.image = UIImage(named: "profileeee") as UIImage?
               
                print("No Image")
            }else{
                let newUrl = googleImage
                // let url = "http://apporio.co.uk/apporiotaxi/\(drivertypeimage!)"
                print(newUrl)
                
                let url1 = NSURL(string: newUrl)
                
                
                userimageview!.af_setImage(withURL:
                    url1! as URL,
                                                 placeholderImage: UIImage(named: "dress"),
                                                 filter: nil,
                                                 imageTransition: .crossDissolve(1.0))
                
            }
            
            
            self.username.text = googleFirstName
             self.useremail.text = googleMail
        }
        else
        {
            
            
            if(facebookImage == ""){
                userimageview.image = UIImage(named: "profileeee") as UIImage?
                
                print("No Image")
            }else{
                let newUrl = facebookImage
                // let url = "http://apporio.co.uk/apporiotaxi/\(drivertypeimage!)"
                print(newUrl)
                
                let url1 = NSURL(string: newUrl)
                
                
                userimageview!.af_setImage(withURL:
                    url1! as URL,
                                           placeholderImage: UIImage(named: "dress"),
                                           filter: nil,
                                           imageTransition: .crossDissolve(1.0))
                
            }

            
            self.username.text = facebookFirstName
            self.useremail.text = facebookMail
        }
        
        
       // self.signoutTwiterDegit()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if GlobalVarible.checkphonenumber == 1{
            
            //self.phoneButton.setTitle(GlobalVarible.enteruserphonenumber, for: .normal)
           // self.phoneButton.setTitleColor(UIColor.black, for: .normal)
            GlobalVarible.checkphonenumber = 0
            
        }else{
            
        }
        
    }

    @IBAction func Selectcountrycode_btn(_ sender: Any) {
        
        let picker = MICountryPicker { (name, code) -> () in
            print(code)
        }
        
        picker.delegate = self
        let backButton = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonTapped))
        picker.navigationItem.leftBarButtonItem = backButton
        // Display calling codes
        picker.showCallingCodes = true
        
        // or closure
        picker.didSelectCountryClosure = { name, code in
        }
        
        // self.present(picker, animated: true, completion: nil)
        let navcontroller = UINavigationController(rootViewController: picker)
        
        self.present(navcontroller,animated: true,completion: nil)
        
    }
    
    func backButtonTapped() {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func backbtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onRegister(_ sender: Any) {
        
        self.view.endEditing(true)
        
        
    
        
        if (self.enterphonetext.text?.isEmpty)! {
            self.showalert(message: "Please Enter Mobile Number First".localized)
           
            return
        }
 
        if movedFrom == "facebook" {
            GlobalVarible.enteruserphonenumber = selcetcountrycode + self.enterphonetext.text!
            let dic=[ facebookSignupUrl2:"\(self.facebookId)",
                facebookSignupUrl3:"\(self.facebookMail)",
                facebookSignupUrl4:"\(self.facebookImage)",
                facebookSignupUrl5:"\(facebookFirstName)",
                facebookSignupUrl6:"\(facebookLastName)",
                facebookSignupUrl7:"\(GlobalVarible.enteruserphonenumber)",
                facebookSignupUrl8:"\(facebookFirstName)",
                facebookSignupUrl9:"\(facebookLastName)",
                facebookSignupUrl10:"\(GlobalVarible.languagecode)"
            ]
            
            for items in dic{
                print(items.1)
            }
            
            print(dic)
            ApiManager.sharedInstance.protocolmain_Catagory = self
            ApiManager.sharedInstance.postData(dictonary: dic as NSDictionary, url: facebookSignupUrl)
            
            //  ApiController.sharedInstance.parsPostData(dic, url:facebookSignupUrl, reseltCode: 10)
            
        }
        
        
        if movedFrom == "google"{
                 GlobalVarible.enteruserphonenumber = selcetcountrycode + self.enterphonetext.text!
            let dic=[ googleSignupUrl2:"\(self.googleId)",
                googleSignupUrl3:"\(googleFirstName)",
                googleSignupUrl4:"\(self.googleMail)",
                googleSignupUrl5:"\(self.googleImage)",
                googleSignupUrl6:"\(selcetcountrycode + self.enterphonetext.text!)",
                googleSignupUrl7:"\(googleFirstName)",
                googleSignupUrl8:"\(googleLastName)",
                googleSignupUrl9:"\(GlobalVarible.languagecode)",
                googleSignupUrl10:"\(self.googleMail)",
                
            ]
            ApiManager.sharedInstance.protocolmain_Catagory = self
            ApiManager.sharedInstance.postData(dictonary: dic as NSDictionary, url: googleSignupUrl)
            
            //  ApiController.sharedInstance.parsPostData(dic, url:googleSignupUrl, reseltCode: 10)
            
        }
        
        
        

    }
   
    
    @IBAction func onPhone(_ sender: Any) {
//
//        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//        let verifyViewController = storyBoard.instantiateViewController(withIdentifier: "VerifyPhoneViewController") as! VerifyPhoneViewController
//        verifyViewController.matchString = ""
//        self.present(verifyViewController, animated:true, completion:nil)

    }
    
    
   
    
    func showalert(message:String)  {
        
        DispatchQueue.main.async(execute: {
            
            let alertController = UIAlertController(title:   "Alert".localized, message:message, preferredStyle: .alert)
            
            
            let OKAction = UIAlertAction(title: "ok".localized, style: .default) { (action) in
                
            }
            alertController.addAction(OKAction)
            
            self.present(alertController, animated: true) {
                
            }
            
            
        })
        
    }
    
    
    
    
    func onProgressStatus(value: Int) {
        if(value == 0 ){
            MBProgressHUD.hide(for: self.view, animated: true)
        }else if (value == 1){
            let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
            spinnerActivity.label.text = "Loading".localized
            spinnerActivity.detailsLabel.text = "Please Wait!!".localized
            spinnerActivity.isUserInteractionEnabled = false
            
        }
    }
    
    func onSuccessExecution(msg: String) {
        print("\(msg)")
    }
    
    
    func onerror(msg : String) {
        MBProgressHUD.hide(for: self.view, animated: true)
        
        self.showalert(message: msg)
        
        
    }
    
    
    func onSuccessParse(data: AnyObject) {
        
        if let logindata = data as? SignupLoginResponse{
            
            self.logindata = logindata

        
        if logindata.result == 1{
            
            let userid = logindata.details!.userId
            
            var UserDeviceKey = ""
            
            let UserDeviceKey1 = UserDefaults.standard.string(forKey: "device_key")
            
            if UserDeviceKey1 == nil{
                
                UserDeviceKey = ""
                
            }else{
                
                UserDeviceKey = UserDeviceKey1!
                
            }
            
            
            var uniqueid = ""
            
            
            let uniqueid1 =  UserDefaults.standard.string(forKey: "unique_number")
            
            if uniqueid1 == nil{
                
                uniqueid = ""
            }else{
                
                uniqueid = uniqueid1!
                
            }
            
            
            ApiManager.sharedInstance.protocolmain_Catagory = self
            ApiManager.sharedInstance.UserDeviceId(USERID: userid!, USERDEVICEID: UserDeviceKey , FLAG: "3",UNIQUEID: uniqueid)
            
            NsUserDekfaultManager.SingeltionInstance.loginuser(user_id: self.logindata.details!.userId!,name: self.logindata.details!.userName!, image: (self.logindata.details?.userImage)!, email: self.logindata.details!.userEmail!, phonenumber: (self.logindata.details?.userPhone!)!, status: self.logindata.details!.status!,password: self.logindata.details!.userPassword!,facbookimage: (self.logindata.details?.facebookImage!)!, googleimage: (self.logindata.details?.googleImage)!)
            
                       
          /* let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let revealViewController:MapViewController = storyBoard.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
            
            self.present(revealViewController, animated:true, completion:nil)*/
            
            let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextController: MapViewController = storyboard.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
            
            if let window = self.view.window{
                window.rootViewController = nextController
            }
            

            
            
        }else{
            
            
        }
        
        }
    }

}

extension SignupViewControllerWithFacebookGoogle: MICountryPickerDelegate {
    func countryPicker(_ picker: MICountryPicker, didSelectCountryWithName name: String, code: String) {
        
    }
    func countryPicker(_ picker: MICountryPicker, didSelectCountryWithName name: String, code: String, dialCode: String)
    {
        selcetcountrycode = dialCode
        countrycodetext.text = dialCode
        self.dismiss(animated: true, completion: nil)
        
        
    }
}
