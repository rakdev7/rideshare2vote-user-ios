//
//  SplashViewController.swift
//  TaxiUser
//
//  Created by AppOrio on 22/05/17.
//  Copyright © 2017 apporio. All rights reserved.
//

import UIKit
import SwiftyJSON
import GoogleMaps
import MapKit
import Firebase


class SplashViewController: UIViewController,CLLocationManagerDelegate,MainCategoryProtocol {
    
    
     var locationManager = CLLocationManager()
    
    
    
     var timer:Timer!
    
    var RANDOMNO = String()
    
    var appupdateData: AppUpdateModel!
    
    
    @IBOutlet weak var gettinglocationlabel: UILabel!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        gettinglocationlabel.text = "getting location....".localized
        
              
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        // locationManager.requestWhenInUseAuthorization()
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.startUpdatingLocation()
        


        if(UserDefaults.standard.object(forKey: "PreferredLanguage") as! String == "en"){
            
            if GlobalVarible.languagecodeselectinmenu == 0{
                GlobalVarible.languagecode = "en"
                
                UserDefaults.standard.set("en", forKey: "PreferredLanguage")
                GlobalVarible.languageid = 1
                Language.language = Language(rawValue: "en")!
            }else{
                
            }

        }else if(UserDefaults.standard.object(forKey: "PreferredLanguage") as! String == "es"){
            
            if GlobalVarible.languagecodeselectinmenu == 0{
                GlobalVarible.languagecode = "es"
                
                UserDefaults.standard.set("es", forKey: "PreferredLanguage")
                GlobalVarible.languageid = 1
                Language.language = Language(rawValue: "es")!
            }else{
                
            }
        }
//        }else if(UserDefaults.standard.object(forKey: "PreferredLanguage") as! String == "fr"){
//
//            if GlobalVarible.languagecodeselectinmenu == 0{
//                GlobalVarible.languagecode = "fr"
//
//                UserDefaults.standard.set("fr", forKey: "PreferredLanguage")
//                GlobalVarible.languageid = 1
//                Language.language = Language(rawValue: "fr")!
//            }else{
//
//            }
//
//        }else if(UserDefaults.standard.object(forKey: "PreferredLanguage") as! String == "pt"){
//
//            if GlobalVarible.languagecodeselectinmenu == 0{
//                GlobalVarible.languagecode = "pt"
//
//                UserDefaults.standard.set("pt", forKey: "PreferredLanguage")
//                GlobalVarible.languageid = 1
//                Language.language = Language(rawValue: "pt")!
//            }else{
//
//            }
//
//        }else if(UserDefaults.standard.object(forKey: "PreferredLanguage") as! String == "tr"){
//
//            if GlobalVarible.languagecodeselectinmenu == 0{
//                GlobalVarible.languagecode = "tr"
//
//                UserDefaults.standard.set("tr", forKey: "PreferredLanguage")
//                GlobalVarible.languageid = 1
//                Language.language = Language(rawValue: "tr")!
//            }else{
//
//            }
//
//        }
//        else{
//            if GlobalVarible.languagecodeselectinmenu == 0{
//                UserDefaults.standard.set("ar", forKey: "PreferredLanguage")
//
//
//                GlobalVarible.languagecode = "ar"
//                GlobalVarible.languageid = 2
//                Language.language = Language(rawValue: "ar")!
//            }else{
//
//            }
//
//        }
        // self.getCurrentAddress()
        
        
        ApiManager.sharedInstance.protocolmain_Catagory = self
        ApiManager.sharedInstance.AppUdateMethod(ApplicationVersion: GlobalVarible.appversion)
        

        
     
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
    
    
   
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            
            reverseGeocodeCoordinate(coordinate: location.coordinate)
            GlobalVarible.PickUpLat = String(location.coordinate.latitude)
            GlobalVarible.PickUpLng = String(location.coordinate.longitude)
            locationManager.stopUpdatingLocation()
            
        }
        
    }
    

    
    
    
    func reverseGeocodeCoordinate(coordinate: CLLocationCoordinate2D)  {
        
        // 1
        
        let geocoder = GMSGeocoder()
        
        // 2
        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
            if let address = response?.firstResult() {
                
                
                //  print(address.lines)
                let lines = address.lines
                
                GlobalVarible.Pickuptext = lines!.joined(separator: "\n")
                
                
                print(GlobalVarible.Pickuptext)
                
                if let city = address.locality{
                    GlobalVarible.usercityname  = String(city)
                    
                }
                else{
                    GlobalVarible.usercityname = "Dummy City"
                    
                }
                
                
                
            }
        }
    }
    
    func myPerformeCode(timer : Timer) {
        
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
    let revealViewController = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        
              self.present(revealViewController, animated:true, completion:nil)
        
            }
    
    func myPerformeCode1(timer : Timer) {
        
        
       /* let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let revealViewController:MapViewController = storyBoard.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        
        self.present(revealViewController, animated:true, completion:nil)*/
        
        GlobalVarible.locationdidactive = 1
        
        
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextController: MapViewController = storyboard.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        
        if let window = self.view.window{
            window.rootViewController = nextController
        }
        
              
        
        
    }
    
    func showalert(message:String)  {
        
        DispatchQueue.main.async(execute: {
            
            let alertController = UIAlertController(title:   "Alert".localized, message:message, preferredStyle: .alert)
            
            
            let OKAction = UIAlertAction(title: "ok".localized, style: .default) { (action) in
                
            }
            alertController.addAction(OKAction)
            
            self.present(alertController, animated: true) {
                
            }
            
            
        })
        
    }
    
    
    func onProgressStatus(value: Int) {
        if(value == 0 ){
            MBProgressHUD.hide(for: self.view, animated: true)
        }else if (value == 1){
            let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
            spinnerActivity.label.text = "Loading".localized
            spinnerActivity.detailsLabel.text = "Please Wait!!".localized
            spinnerActivity.isUserInteractionEnabled = false
            
        }
    }
    
    func onSuccessExecution(msg: String) {
        print("\(msg)")
    }
    
    
    func onerror(msg : String) {
        MBProgressHUD.hide(for: self.view, animated: true)
        
            self.showalert(message: msg)
                
    }
    
    
    
    func onSuccessParse(data: AnyObject) {
        
        
        if (GlobalVarible.Api == "appupdateData") {
            
            if let appupdateData = data as? AppUpdateModel{
            
            self.appupdateData = appupdateData
           
            
            if(self.appupdateData.appcheck == 1){
                
                 GlobalVarible.playerid = self.appupdateData.playerid!
                
                 GlobalVarible.updatechecklater = self.appupdateData.appcheck!
                
                if(self.appupdateData.details?.iosUserMaintenanceMode == "1"){
                
                    
                    let alert = UIAlertController(title: "", message: "App is under maintance.".localized, preferredStyle: .alert)
                    let action = UIAlertAction(title: "Exit".localized, style: .default) { _ in
                        exit(0)
                    }
                    alert.addAction(action)
                    self.present(alert, animated: true){}
 
                }else{
                
                
                let message: String = "New Version Available. Please update the app first.".localized
                 let alertController = UIAlertController(
                 title: "UPDATE AVAILABLE ".localized, // This gets overridden below.
                 message: message,
                 preferredStyle: .alert
                 )
                 let okAction = UIAlertAction(title: "Update App".localized, style: .cancel) { _ -> Void in
                 
                    GlobalVarible.rateApp(appId: "id1439888914") { success in
                        
                    }
                 
                 
                 }
                 alertController.addAction(okAction)
                 
                 
                  self.present(alertController, animated: true, completion: nil)
                
                
                }
                
            }
                
            else if(self.appupdateData.appcheck == 2){
                
                
                if(self.appupdateData.details?.iosUserMaintenanceMode == "1"){
                    
                    
                    
                    let alert = UIAlertController(title: "", message: "App is under maintance.".localized, preferredStyle: .alert)
                    let action = UIAlertAction(title: "Exit".localized, style: .default) { _ in
                        exit(0)
                    }
                    alert.addAction(action)
                    self.present(alert, animated: true){}

                }else{

                
                
                
                let refreshAlert = UIAlertController(title: "UPDATE AVAILABLE ".localized, message: "New Version Available. Please update the app.".localized, preferredStyle: UIAlertControllerStyle.alert)
                
                refreshAlert.addAction(UIAlertAction(title: "Update App".localized, style: .default, handler: { (action: UIAlertAction!) in
                    
                    GlobalVarible.rateApp(appId: "id1439888914") { success in
                        
                    }

                    
                    
                   
                    
                }))
                
                refreshAlert.addAction(UIAlertAction(title: "Later".localized, style: .default, handler: { (action: UIAlertAction!) in
                
                    if(NsUserDekfaultManager.SingeltionInstance.isloggedin()){
                        
                        
                        
                        let userid = NsUserDekfaultManager.SingeltionInstance.getuserdetaild(key: NsUserDekfaultManager.Keyuserid)
                        
                        var UserDeviceKey = ""
                        
                        let UserDeviceKey1 = UserDefaults.standard.string(forKey: "device_key")
                        
                        if UserDeviceKey1 == nil{
                            
                            UserDeviceKey = ""
                            
                        }else{
                            
                            UserDeviceKey = UserDeviceKey1!
                            
                        }
                        
                        
                        var uniqueid = ""
                        
                        
                        let uniqueid1 =  UserDefaults.standard.string(forKey: "unique_number")
                        
                        if uniqueid1 == nil{
                            
                            uniqueid = ""
                        }else{
                            
                            uniqueid = uniqueid1!
                            
                        }
                        
                        
                        ApiManager.sharedInstance.protocolmain_Catagory = self
                        ApiManager.sharedInstance.UserDeviceId(USERID: userid!, USERDEVICEID: UserDeviceKey , FLAG: "3",UNIQUEID: uniqueid)
                        
                        
                        self.timer  = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(SplashViewController.myPerformeCode1), userInfo: nil, repeats: false)
                        
                        
                    }
                    else{
                        
                        
                      
                        
                        self.timer  = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(SplashViewController.myPerformeCode), userInfo: nil, repeats: false)
                        
                    }

                
                }))
                
                present(refreshAlert, animated: true, completion: nil)
                
                

                }
                
                }
                
            else{
                
                
                if(self.appupdateData.details?.iosUserMaintenanceMode == "1"){
                    
                    
                    
                    
                    let alert = UIAlertController(title: "", message: "App is under maintance.".localized, preferredStyle: .alert)
                    let action = UIAlertAction(title: "Exit".localized, style: .default) { _ in
                        exit(0)
                    }
                    alert.addAction(action)
                    self.present(alert, animated: true){}

                    
                }else{

                
                if(NsUserDekfaultManager.SingeltionInstance.isloggedin()){
                    
                    
                    
                    let userid = NsUserDekfaultManager.SingeltionInstance.getuserdetaild(key: NsUserDekfaultManager.Keyuserid)
                    
                    var UserDeviceKey = ""
                    
                    let UserDeviceKey1 = UserDefaults.standard.string(forKey: "device_key")
                    
                    if UserDeviceKey1 == nil{
                        
                        UserDeviceKey = ""
                        
                    }else{
                        
                        UserDeviceKey = UserDeviceKey1!
                        
                    }
                    
                    
                    var uniqueid = ""
                    
                    
                    let uniqueid1 =  UserDefaults.standard.string(forKey: "unique_number")
                    
                    if uniqueid1 == nil{
                        
                        uniqueid = ""
                    }else{
                        
                        uniqueid = uniqueid1!
                        
                    }
                    
                    
                    ApiManager.sharedInstance.protocolmain_Catagory = self
                    ApiManager.sharedInstance.UserDeviceId(USERID: userid!, USERDEVICEID: UserDeviceKey , FLAG: "3",UNIQUEID: uniqueid)
                    
                    
                    timer  = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(SplashViewController.myPerformeCode1), userInfo: nil, repeats: false)
                    
                    
                }
                else{
                    
                    
                    // self.getCurrentAddress()
                    
                    //  randomStringWithLength(len: 15)
                    
                    timer  = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(SplashViewController.myPerformeCode), userInfo: nil, repeats: false)
                    
                }
                
                }
                
            }
            
            }
        }
        

        
    }



   
}
