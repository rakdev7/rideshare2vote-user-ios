//
//  Details.swift
//
//  Created by Apporio on 29/03/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class FavoriteLocDetailModel {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let lng = "lng"
    static let location = "location"
    static let favouriteLocationId = "favourite_location_id"
    static let otherName = "other_name"
    static let userId = "user_id"
    static let lat = "lat"
    static let category = "category"
  }

  // MARK: Properties
  public var lng: String?
  public var location: String?
  public var favouriteLocationId: String?
  public var otherName: String?
  public var userId: String?
  public var lat: String?
  public var category: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    lng = json[SerializationKeys.lng].string
    location = json[SerializationKeys.location].string
    favouriteLocationId = json[SerializationKeys.favouriteLocationId].string
    otherName = json[SerializationKeys.otherName].string
    userId = json[SerializationKeys.userId].string
    lat = json[SerializationKeys.lat].string
    category = json[SerializationKeys.category].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = lng { dictionary[SerializationKeys.lng] = value }
    if let value = location { dictionary[SerializationKeys.location] = value }
    if let value = favouriteLocationId { dictionary[SerializationKeys.favouriteLocationId] = value }
    if let value = otherName { dictionary[SerializationKeys.otherName] = value }
    if let value = userId { dictionary[SerializationKeys.userId] = value }
    if let value = lat { dictionary[SerializationKeys.lat] = value }
    if let value = category { dictionary[SerializationKeys.category] = value }
    return dictionary
  }

}
