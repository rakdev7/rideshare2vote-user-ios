//
//  SettingViewController.swift
//  Apporio Taxi
//
//  Created by Atul Jain on 02/04/18.
//  Copyright © 2018 apporio. All rights reserved.
//

import UIKit
import MessageUI

class SettingViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,MainCategoryProtocol,MFMailComposeViewControllerDelegate {
    
    
    var reportissuedata: ReportIssueModel!
    @IBOutlet weak var settingtableview: UITableView!
    
    var dataArray: [String] = ["Terms & Conditions".localized,"About Us".localized,"Version".localized + "  (" + GlobalVarible.appversion + ")"]
    
    
    @IBOutlet weak var topsettingtext: UILabel!
    
    
    @IBOutlet weak var appsettingstextlbl: UILabel!
    
    
    @IBOutlet weak var supporttextlbl: UILabel!
    @IBOutlet weak var changelanguagetextlbl: UILabel!
    
    
    @IBOutlet weak var informationtextlbl: UILabel!
    @IBOutlet weak var customersupporttextlbl: UILabel!
    
    @IBOutlet weak var reportissuetextlbl: UILabel!
    
   // var imageArray: [String] = ["ic_book","ic_trips","ic_tag_us_dollar","ic_payment-2-1","ic_payment","refer_earn","icons8-Notification Filled-50","LanguageIcon","system_report","missed_call","ic_terms_condition","ic_about_us","icons8-Logout Rounded Up-48"]
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        topsettingtext.text = "Settings".localized
        appsettingstextlbl.text = "APP SETTINGS".localized
        changelanguagetextlbl.text = "Change Language".localized
        supporttextlbl.text = "SUPPORT".localized
        customersupporttextlbl.text = "Customer Support".localized
        reportissuetextlbl.text = "Report Issue".localized
        informationtextlbl.text = "INFORMATION".localized


        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backbtnclick(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
        mailComposerVC.setToRecipients([GlobalVarible.ContactEmail])
        mailComposerVC.setSubject("Report Issue Regarding TaxiUser App".localized)
        mailComposerVC.setMessageBody("Sending e-mail in-app is not so bad!".localized, isHTML: false)
        
        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
        let sendMailErrorAlert = UIAlertView(title: "Could Not Send Email".localized, message: "Your device could not send e-mail.  Please check e-mail configuration and try again.".localized, delegate: self, cancelButtonTitle: "ok".localized)
        sendMailErrorAlert.show()
    }
    
    // MARK: MFMailComposeViewControllerDelegate Method
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    

    
    
    @IBAction func changelanguagebtnclick(_ sender: Any) {
        
        
        let alertView = UIAlertController(title: "Select Language".localized, message: "Please Choose Language".localized, preferredStyle: .alert)
        let langEnglish = UIAlertAction(title: "English", style: .default, handler: { (alert) in
            
            GlobalVarible.languagecodeselectinmenu = 1
            
            GlobalVarible.languagecode = "en"
            
            UserDefaults.standard.set("en", forKey: "PreferredLanguage")
            Language.language = Language(rawValue: "en")!
            UserDefaults.standard.synchronize()
            
        })
        let langSpanish = UIAlertAction(title: "Spanish", style: .default, handler: { (alert) in
            
            GlobalVarible.languagecodeselectinmenu = 1
            
            GlobalVarible.languagecode = "es"
            
            UserDefaults.standard.set("es", forKey: "PreferredLanguage")
            Language.language = Language(rawValue: "es")!
            UserDefaults.standard.synchronize()
            
        })
//        let langFrench = UIAlertAction(title: "French", style: .default, handler: { (alert) in
//
//            GlobalVarible.languagecodeselectinmenu = 1
//
//            GlobalVarible.languagecode = "fr"
//
//            UserDefaults.standard.set("fr", forKey: "PreferredLanguage")
//            Language.language = Language(rawValue: "fr")!
//            UserDefaults.standard.synchronize()
//
//        })
//        let langPortuguese = UIAlertAction(title: "Portuguese", style: .default, handler: { (alert) in
//
//            GlobalVarible.languagecodeselectinmenu = 1
//
//            GlobalVarible.languagecode = "pt"
//
//            UserDefaults.standard.set("pt", forKey: "PreferredLanguage")
//            Language.language = Language(rawValue: "pt")!
//            UserDefaults.standard.synchronize()
//
//        })
//        let langTurkish = UIAlertAction(title: "Turkish", style: .default, handler: { (alert) in
//
//            GlobalVarible.languagecodeselectinmenu = 1
//
//            GlobalVarible.languagecode = "tr"
//
//            UserDefaults.standard.set("tr", forKey: "PreferredLanguage")
//            Language.language = Language(rawValue: "tr")!
//            UserDefaults.standard.synchronize()
//
//        })
//        let langArabic = UIAlertAction(title: "عربى", style: .default, handler: { (alert) in
//            GlobalVarible.languagecodeselectinmenu = 1
//            UserDefaults.standard.set("ar", forKey: "PreferredLanguage")
//              GlobalVarible.languagecode = "ar"
//            Language.language = Language(rawValue: "ar")!
//
//        })
        let cancelAction = UIAlertAction(title: "Cancel".localized,
                                         style: .cancel, handler: nil)
        alertView.addAction(langEnglish)
        alertView.addAction(langSpanish)
     //   alertView.addAction(langFrench)
//        alertView.addAction(langPortuguese)
//        alertView.addAction(langTurkish)
//        alertView.addAction(langArabic)
        alertView.addAction(cancelAction)
        self.present(alertView, animated: true, completion: nil)
        

    }
    
    
    @IBAction func customersupportbtnclick(_ sender: Any) {
        
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextController: CustomerSupportViewController = storyboard.instantiateViewController(withIdentifier: "CustomerSupportViewController") as! CustomerSupportViewController
        self.present(nextController, animated: true, completion: nil)
        
        

    }
    
    
    @IBAction func reportissuebtnclick(_ sender: Any) {
        
        ApiManager.sharedInstance.protocolmain_Catagory = self
        ApiManager.sharedInstance.ReportIssueMethod()

    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return dataArray.count
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = settingtableview.dequeueReusableCell(withIdentifier: "settingcell", for: indexPath)
        
        
        let label : UILabel = (cell.contentView.viewWithTag(1) as? UILabel)!
        let labelshow : UILabel = (cell.contentView.viewWithTag(2) as? UILabel)!
        
        labelshow.isHidden = false
            
        label.text = dataArray[indexPath.row]
        
        
        
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        settingtableview.deselectRow(at: indexPath as IndexPath, animated: true)
        let row = indexPath.row
        print("Row: \(row)")
        
        if(indexPath.row == 0){
            
           
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let termsViewController = storyBoard.instantiateViewController(withIdentifier: "TemsConditionsViewController") as! TemsConditionsViewController
            self.present(termsViewController, animated:true, completion:nil)
            //  self.revealViewController().revealToggleAnimated(true)
            
            
            
        }
        if(indexPath.row == 1){
            
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let aboutViewController = storyBoard.instantiateViewController(withIdentifier: "AboutusViewController") as! AboutusViewController
            self.present(aboutViewController, animated:true, completion:nil)
            //  self.revealViewController().revealToggleAnimated(true)
            
            
        }

        
        
        if(indexPath.row == 2){
            
            
        }
        
    }
    
    func showalert(message:String)  {
        
        DispatchQueue.main.async(execute: {
            
            let alertController = UIAlertController(title:   "Alert".localized, message:message, preferredStyle: .alert)
            
            
            let OKAction = UIAlertAction(title: "ok".localized, style: .default) { (action) in
                
            }
            alertController.addAction(OKAction)
            
            self.present(alertController, animated: true) {
                
            }
            
            
        })
        
    }
    
    
    
    
    func onProgressStatus(value: Int) {
        if(value == 0 ){
            MBProgressHUD.hide(for: self.view, animated: true)
        }else if (value == 1){
            let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
            spinnerActivity.label.text = "Loading".localized
            spinnerActivity.detailsLabel.text = "Please Wait!!".localized
            spinnerActivity.isUserInteractionEnabled = false
            
        }
    }
    
    func onSuccessExecution(msg: String) {
        print("\(msg)")
    }
    
    
    func onerror(msg : String) {
        MBProgressHUD.hide(for: self.view, animated: true)
        
        self.showalert(message: msg)
        
    }
    
    
    
    func onSuccessParse(data: AnyObject) {
        
        
        if(GlobalVarible.Api == "reportissue"){
            
            
            if let reportissuedata = data as? ReportIssueModel{
                
                self.reportissuedata = reportissuedata
            
            if(reportissuedata.result == 1){
                
                GlobalVarible.ContactEmail = reportissuedata.deatils!
                
                let mailComposeViewController = configuredMailComposeViewController()
                if MFMailComposeViewController.canSendMail() {
                    self.present(mailComposeViewController, animated: true, completion: nil)
                } else {
                    self.showSendMailErrorAlert()
                }
                
                
                
            }else{
                
                
                
                let mailComposeViewController = configuredMailComposeViewController()
                if MFMailComposeViewController.canSendMail() {
                    self.present(mailComposeViewController, animated: true, completion: nil)
                } else {
                    self.showSendMailErrorAlert()
                }
                
                
            }
            
            
        }
        
        }
        

        
    }
    

    
}
