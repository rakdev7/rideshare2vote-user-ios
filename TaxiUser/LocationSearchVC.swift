
//
//  LocationSearchVC.swift
//  Apporio Taxi
//
//  Created by Apporio on 12/03/18.
//  Copyright © 2018 apporio. All rights reserved.
//

import UIKit
import GooglePlaces

class LocationSearchVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate,MainCategoryProtocol, GMSAutocompleteViewControllerDelegate{
    
    @IBOutlet weak var toplocationtextlbl: UILabel!
    
    @IBOutlet weak var tableViewfav: UITableView!
    
    var toastLabel: UILabel!
    var locationViewAdded:FavoriteLocModel!
    
    
    var userPickupLat = "0.0"
    var userPickupLong = "0.0"
   
    var strSelectAddress = ""
    
    var userDropLat = 0.0
    var userDropLong = 0.0
    
    
    @IBOutlet weak var txtLocationPlace: UITextField!
    var deleteData: Coupons!
    
    var tablesize = 0
    
    @IBOutlet weak var donebtntext: UIButton!
    @IBOutlet weak var favouritestextlbl: UILabel!
    let Userid = NsUserDekfaultManager.SingeltionInstance.getuserdetaild(key: NsUserDekfaultManager.Keyuserid)
    
    var getIndex = ""
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        toplocationtextlbl.text = "LOCATION".localized
        favouritestextlbl.text = "Favourites".localized
        
        txtLocationPlace.placeholder = "Search Location .....".localized
        donebtntext.setTitle("Done".localized, for: UIControlState.normal)
        
        toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 150, y: self.view.frame.size.height-300, width: 300, height: 35))
        toastLabel.backgroundColor = UIColor.white
        toastLabel.textColor = UIColor.black
        toastLabel.textAlignment = NSTextAlignment.center;
        self.view.addSubview(toastLabel)
        toastLabel.text =  "No Favorite Locations!".localized
        
        toastLabel.isHidden = true
        
        self.automaticallyAdjustsScrollViewInsets = true
        
       
        
        ApiManager.sharedInstance.protocolmain_Catagory = self
        ApiManager.sharedInstance.LocationAdded(UserId: Userid!)
        
        
        
        
    }
    
    
    // MARK: - Table view datasource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tablesize
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellLocation") as! CellLocation
        
        cell.lblAddress.text = locationViewAdded.details?[indexPath.row].location
        
        if locationViewAdded.details?[indexPath.row].category == "1" {
            
            cell.imgLocationType?.image = UIImage(named: "Home-25 (3)")
            cell.imgLocationType?.clipsToBounds = true
            cell.lblLocationType.text = "Home"
            
        } else if locationViewAdded.details?[indexPath.row].category == "2" {
            
            cell.imgLocationType?.image = UIImage(named: "WorkImage")
            cell.imgLocationType?.clipsToBounds = true
            cell.lblLocationType.text = "Work"

            
        } else {
         
            cell.imgLocationType?.image = UIImage(named: "OtherLocation")
            cell.imgLocationType?.clipsToBounds = true
            cell.lblLocationType.text = locationViewAdded.details?[indexPath.row].otherName

            
        }
       
        cell.btnDelete.tag = indexPath.row
        cell.btnDelete.addTarget(self, action: #selector(ClickOnRemoveFavorite) , for: .touchUpInside)
        return cell

        
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        GlobalVarible.newchecklocationvalue = "0"

        if getIndex == "1" {
            
            GlobalVarible.PickUpLat = (self.locationViewAdded.details?[indexPath.row].lat)!
            GlobalVarible.PickUpLng = (self.locationViewAdded.details?[indexPath.row].lng)!
            GlobalVarible.Pickuptext = (self.locationViewAdded.details?[indexPath.row].location)!
            GlobalVarible.checklocationvalue = 3
            GlobalVarible.checkfavvalue = 3
            self.dismiss(animated: true, completion: nil)
        }
        
        if getIndex == "2" {
            
            GlobalVarible.UserDropLat = Double((self.locationViewAdded.details?[indexPath.row].lat)!)!
            GlobalVarible.UserDropLng = Double((self.locationViewAdded.details?[indexPath.row].lng)!)!
            GlobalVarible.UserDropLocationText = (self.locationViewAdded.details?[indexPath.row].location)!
            GlobalVarible.checklocationvalue = 2
            GlobalVarible.checkfavvalue = 2
            self.dismiss(animated: true, completion: nil)
        }
        
        
        
        
    }
    
    
    @IBAction func backButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
    func ClickOnRemoveFavorite(sender:AnyObject){
        
        let alertController = UIAlertController(title: "Alert!".localized, message: "Are you sure to unfavorite this location?".localized, preferredStyle: UIAlertControllerStyle.alert)
        
        //CREATING OK BUTTON
        
        let OKAction = UIAlertAction(title: "YES".localized, style: .default) { (action:UIAlertAction!) in
            
            print(sender.tag)
            
            let favoriteLocationID  = self.locationViewAdded.details?[sender.tag].favouriteLocationId
            ApiManager.sharedInstance.protocolmain_Catagory = self
            ApiManager.sharedInstance.LocationRemove(favoriteId: favoriteLocationID!)
            
        }
        alertController.addAction(OKAction)
        
        // Create Cancel button
        let cancelAction = UIAlertAction(title: "NO".localized, style: .cancel) { (action:UIAlertAction!) in
            print("Cancel button tapped");
        }
        alertController.addAction(cancelAction)
        
        // Present Dialog message
        self.present(alertController, animated: true, completion:nil)
    }
    

    
    
    //MARk: - Text Delegate 
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: false, completion: nil)
        
        
        
        return true
    }
    
    
    @IBAction func DoneButton(_ sender: Any) {
        
        
        if userPickupLat == "0.0" && userDropLat == 0.0{
            
            GlobalVarible.newchecklocationvalue = "1"
         dismiss(animated: true, completion: nil)
            
        }else{
        
         GlobalVarible.newchecklocationvalue = "0"
        
        if getIndex == "1" {
            
            GlobalVarible.PickUpLat = self.userPickupLat
            GlobalVarible.PickUpLng = self.userPickupLong
              GlobalVarible.checklocationvalue = 3
            GlobalVarible.Pickuptext = self.strSelectAddress
            
        }
        
        if getIndex == "2" {
         
            GlobalVarible.UserDropLat = self.userDropLat
            GlobalVarible.UserDropLng = self.userDropLong
            GlobalVarible.UserDropLocationText = self.strSelectAddress
            
            GlobalVarible.checklocationvalue = 2
            
            
        }
        
     dismiss(animated: true, completion: nil)
        
        }
    }
    
    
    //MARK: -  Protocols
    
    
    func onProgressStatus(value: Int) {
        if(value == 0 ){
            MBProgressHUD.hide(for: self.view, animated: true)
        }else if (value == 1){
            let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
            spinnerActivity.label.text = "Loading".localized
            spinnerActivity.detailsLabel.text = "Please Wait!!".localized
            spinnerActivity.isUserInteractionEnabled = false
            
        }
    }
    
    func onSuccessExecution(msg: String) {
        print("\(msg)")
    }
    
    
    func onerror(msg : String) {
        MBProgressHUD.hide(for: self.view, animated: true)
        
        self.showalert(message: msg)
        
    }
    
    
    func onSuccessParse(data: AnyObject) {
        
        if(GlobalVarible.Api == "viewAddedLocation"){
            
            
            if let  locationViewAdded = data as? FavoriteLocModel{
            
                self.locationViewAdded = locationViewAdded
            if locationViewAdded.result == 1{
                
                tablesize = (locationViewAdded.details?.count)!
//                
//                if(tablesize > 0){
//                    
//                    
//                } else{
//                    
//                }
                toastLabel.isHidden = true
            }
                
            else {
                
                tablesize = 0
                toastLabel.isHidden = false
                
            }
            
            self.tableViewfav.reloadData()
            
            }
        }
        
        
        if(GlobalVarible.Api == "removeFavoriteLocation"){
            
            if let deleteData = data as? Coupons{
            
                self.deleteData = deleteData
              if deleteData.result == 1{
                self.showalert(message:deleteData.msg!)
                
                
                ApiManager.sharedInstance.protocolmain_Catagory = self
                ApiManager.sharedInstance.LocationAdded(UserId: Userid!)
                
                
              } else {
                self.showalert(message:deleteData.msg!)
                
            }
            
            }
            
        }
        
    }
    
    
    func showalert(message:String)  {
        
        DispatchQueue.main.async(execute: {
            
            let alertController = UIAlertController(title: "Alert".localized, message:message, preferredStyle: .alert)
            let OKAction = UIAlertAction(title: "ok".localized, style: .default) { (action) in
                
            }
            
            alertController.addAction(OKAction)
            self.present(alertController, animated: true) {
            }
            
            
        })
        
    }
    
}


extension LocationSearchVC {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print(place)
        print("Place name: \(place.name)")
        print("Place address: \(place.formattedAddress)")
        print("Place attributions: \(place.attributions)")
        print("Place attributions: \(place.coordinate.latitude)")
        print("Place attributions: \(place.coordinate.longitude)")
        
        self.txtLocationPlace.text = "\(place.name)"
        //  manualloactioncheck = "manual"
        
        
        strSelectAddress = place.name
        
        
        if getIndex == "1"{
            

            userPickupLat = String(place.coordinate.latitude)
            userPickupLong = String(place.coordinate.longitude)
            
            
            
        }
        
        if getIndex == "2" {
            
               userDropLat = place.coordinate.latitude
               userDropLong = place.coordinate.longitude
            
            
        }
        
        
        if userPickupLat == "0.0" && userDropLat == 0.0{
            
            GlobalVarible.newchecklocationvalue = "1"
            dismiss(animated: true, completion: nil)
            
        }else{
            
            GlobalVarible.newchecklocationvalue = "0"
            
            if getIndex == "1" {
                
                GlobalVarible.PickUpLat = self.userPickupLat
                GlobalVarible.PickUpLng = self.userPickupLong
                GlobalVarible.checklocationvalue = 3
                GlobalVarible.Pickuptext = self.strSelectAddress
                
            }
            
            if getIndex == "2" {
                
                GlobalVarible.UserDropLat = self.userDropLat
                GlobalVarible.UserDropLng = self.userDropLong
                GlobalVarible.UserDropLocationText = self.strSelectAddress
                
                GlobalVarible.checklocationvalue = 2
                
                
            }
        }
        
        presentingViewController?.dismiss(animated: false, completion: nil)
        //dismiss(animated: false, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: false, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}



