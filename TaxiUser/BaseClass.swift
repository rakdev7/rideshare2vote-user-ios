//
//  BaseClass.swift
//
//  Created by Atul Jain on 13/06/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON
public class BaseClass {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let check = "check"
    static let keyThree = "key_three"
    static let keyTwo = "key_two"
    static let heyOne = "hey_one"
  }

  // MARK: Properties
  public var check: Bool? = false
  public var keyThree: Int?
  public var keyTwo: String?
  public var heyOne: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    check = json[SerializationKeys.check].boolValue
    keyThree = json[SerializationKeys.keyThree].int
    keyTwo = json[SerializationKeys.keyTwo].string
    heyOne = json[SerializationKeys.heyOne].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    dictionary[SerializationKeys.check] = check
    if let value = keyThree { dictionary[SerializationKeys.keyThree] = value }
    if let value = keyTwo { dictionary[SerializationKeys.keyTwo] = value }
    if let value = heyOne { dictionary[SerializationKeys.heyOne] = value }
    return dictionary
  }

}
