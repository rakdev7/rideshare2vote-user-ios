//
//  AppColors.swift
//  Apporio Taxi
//
//  Created by apple on 10/22/18.
//  Copyright © 2018 apporio. All rights reserved.
//

import Foundation

struct AppColors {
    
    static let themeColor = UIColor(r: 0, g: 66, b: 130, a: 1)
    static let whiteColor = UIColor(r: 255, g: 255, b: 255, a: 1)
    static let blackColor = UIColor(r: 0, g: 0, b: 0, a: 1)
    
}

extension UIColor {
    
    convenience init(r: CGFloat, g: CGFloat, b: CGFloat, a: CGFloat = 1.0) {
        self.init(red: r / 255.0, green: g / 255.0, blue: b / 255.0, alpha: a)
    }
}
