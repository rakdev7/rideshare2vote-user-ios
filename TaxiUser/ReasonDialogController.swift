//
//  ReasonDialogController.swift
//  TaxiUser
//
//  Created by AppOrio on 24/05/17.
//  Copyright © 2017 apporio. All rights reserved.
//

import UIKit
import Firebase

class ReasonDialogController: UIViewController,UITableViewDataSource,UITableViewDelegate,MainCategoryProtocol  {
    
    var reasonData: ReasonModel!
    
    var usercanceldata: BookingCancelled!
    
    var rentalcanceldata: RentalRideCancelModel!
    
   
     var ref = FIRDatabase.database().reference()
    
  
    var SIZE = 0
    
    var check = 1000
    
     var movefrom = ""
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var cancel_btn: UIButton!
    
    @IBOutlet weak var notCancel_btn: UIButton!
    
    
    @IBOutlet weak var selectreasonlabel: UILabel!
    
     let  Userid =  NsUserDekfaultManager.SingeltionInstance.getuserdetaild(key: NsUserDekfaultManager.Keyuserid)!


    override func viewDidLoad() {
        super.viewDidLoad()

        
        selectreasonlabel.text = "Select Reason".localized
        
        //   NSLocalizedString("Cancel", comment: "")
        
        self.cancel_btn.setTitle("Cancel".localized, for: UIControlState.normal)
        
        self.notCancel_btn.setTitle("Don't Cancel".localized, for: UIControlState.normal)
        
        view.backgroundColor = UIColor.clear
        view.isOpaque = false
        cancel_btn.isEnabled = false
        cancel_btn.layer.backgroundColor = ReasonDialogController.getColorFromHex(hexString: "#979897").cgColor
        ApiManager.sharedInstance.protocolmain_Catagory = self
        ApiManager.sharedInstance.reasonCancel()
        
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    class func getColorFromHex(hexString:String)->UIColor{
        
        var rgbValue : UInt32 = 0
        let scanner:Scanner =  Scanner(string: hexString)
        
        scanner.scanLocation = 1
        scanner.scanHexInt32(&rgbValue)
        
        return UIColor(red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0, green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0, blue: CGFloat(rgbValue & 0x0000FF) / 255.0, alpha: CGFloat(1.0))
    }
    
    @IBAction func donotcancelclickbtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
    @IBAction func cancelclickbtn(_ sender: Any) {
        
        if movefrom == "Rentaltype"{
            GlobalVarible.newtimer?.cancel()
            GlobalVarible.newtimer = nil
            ApiManager.sharedInstance.protocolmain_Catagory = self
            ApiManager.sharedInstance.RentalUserCancel(RentalBookindId: GlobalVarible.RideId, UserId: self.Userid)

        
        }else{
            
            GlobalVarible.newtimer?.cancel()
            GlobalVarible.newtimer = nil
        ApiManager.sharedInstance.protocolmain_Catagory = self
        
        ApiManager.sharedInstance.CancelRide(RIDEID: GlobalVarible.RideId, RIDESTATUS: "2")
        }
        
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
       return SIZE
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "reasonCell", for: indexPath) as! ReasonCell
        cell.selectionStyle = .none
        cell.reasonText.text = self.reasonData.msg![indexPath.row].reasonName!
        
        if(check == indexPath.row)
        {
            
            cell.checkRadioBtn.image = UIImage(named: "Circled Dot-35 (1)")
            
        }else{
            cell.checkRadioBtn.image = UIImage(named: "Circle Thin-35 (1)")
            
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
       // let cells = tableView.cellForRow(at: indexPath) as! ReasonCell
        let row = indexPath.row
        check = indexPath.row
        print("Row:\(row)")
        cancel_btn.isEnabled = true
        GlobalVarible.cancelId = self.reasonData.msg![row].reasonId!
        print(GlobalVarible.cancelId)
        cancel_btn.layer.backgroundColor = ReasonDialogController.getColorFromHex(hexString: "#000000").cgColor
        tableView.reloadData()
        
        
    }
    
    
    func showalert(message:String)  {
        
        DispatchQueue.main.async(execute: {
            
            let alertController = UIAlertController(title:   "Alert".localized, message:message, preferredStyle: .alert)
            
            
            let OKAction = UIAlertAction(title: "ok".localized, style: .default) { (action) in
                
            }
            alertController.addAction(OKAction)
            
            self.present(alertController, animated: true) {
                
            }
            
            
        })
        
    }
    
    
    func showalert2(message:String)  {
        
        DispatchQueue.main.async(execute: {
            
            let alertController = UIAlertController(title:   "Alert".localized, message:message, preferredStyle: .alert)
            
            
            let OKAction = UIAlertAction(title: "ok".localized, style: .default) { (action) in
                self.dismiss(animated: true, completion: nil)
            }
            alertController.addAction(OKAction)
            
            self.present(alertController, animated: true) {
                
            }
            
            
        })
        
    }

    
    
    func showalert1(message:String)  {
        
        DispatchQueue.main.async(execute: {
            
            let alertController = UIAlertController(title:   "Alert".localized, message:message, preferredStyle: .alert)
            
            
            let OKAction = UIAlertAction(title: "ok".localized, style: .default) { (action) in
                
               /* let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let revealViewController:MapViewController = storyBoard.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
                
                self.present(revealViewController, animated:true, completion:nil)*/
                
                let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let nextController: MapViewController = storyboard.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
                
                if let window = self.view.window{
                    window.rootViewController = nextController
                }

                
            }
            alertController.addAction(OKAction)
            
            self.present(alertController, animated: true) {
                
            }
            
            
        })
        
    }
    
    
    
    

    
    
    func onProgressStatus(value: Int) {
        if(value == 0 ){
            MBProgressHUD.hide(for: self.view, animated: true)
        }else if (value == 1){
            let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
            spinnerActivity.label.text = "Loading".localized
            spinnerActivity.detailsLabel.text = "Please Wait!!".localized
            spinnerActivity.isUserInteractionEnabled = false
            
        }
    }
    
    func onSuccessExecution(msg: String) {
        print("\(msg)")
    }
    
    
    func onerror(msg : String) {
        MBProgressHUD.hide(for: self.view, animated: true)
        
        self.showalert(message: msg)
        
    }
    
    
    
    func onSuccessParse(data: AnyObject) {
        
        if(GlobalVarible.Api == "cancelreason"){
            
            if let reasonData = data as? ReasonModel{
                
                self.reasonData = reasonData
            
            
            if reasonData.result == 1{
                
                if let size = reasonData.msg?.count
                {
                    SIZE = size
                    
                }
                
                self.tableView.reloadData()
                
                
            }
                
            }

        }
        if(GlobalVarible.Api == "Cancelbyuser"){
            
            if let usercanceldata = data as? BookingCancelled{
                
                self.usercanceldata = usercanceldata
            
            if(usercanceldata.result == 1){
                GlobalVarible.newtimer?.cancel()
                GlobalVarible.newtimer = nil
                GlobalVarible.checkRideId = "0"
                
                UserDefaults.standard.setValue("0", forKey:"firebaseride_status")
                
                
               
                
                GlobalVarible.UserDropLocationText = "No drop off point".localized
                GlobalVarible.UserDropLat = 0.0
                GlobalVarible.UserDropLng = 0.0
                GlobalVarible.PaymentOptionId = "1"
                GlobalVarible.CouponCode = ""
                GlobalVarible.paymentmethod = "Cash"


                
                self.showalert1(message: self.usercanceldata.msg!)
                
              
                
                //self.dismissViewControllerAnimated(true, completion: nil)
                
            }else{
                
                self.showalert2(message: self.usercanceldata.msg!)
            }
            }
        
        }
        
        if(GlobalVarible.Api == "rentalCancelbyuser"){
            
            if let  rentalcanceldata = data as? RentalRideCancelModel{
                
                self.rentalcanceldata = rentalcanceldata
            
            if(self.rentalcanceldata.status == 1){
                
                GlobalVarible.newtimer?.cancel()
                GlobalVarible.newtimer = nil
                
              
                
                GlobalVarible.UserDropLocationText = "No drop off point".localized
                GlobalVarible.UserDropLat = 0.0
                GlobalVarible.UserDropLng = 0.0
                GlobalVarible.PaymentOptionId = "1"
                GlobalVarible.CouponCode = ""
                GlobalVarible.paymentmethod = "Cash"

            
            self.showalert1(message: self.rentalcanceldata.message!)
                
            }else{
                
            self.showalert2(message: self.rentalcanceldata.message!)
            }
            
            }
        
        }

        
    }


}
