//
//  AllTransactionViewController.swift
//  Apporio Taxi
//
//  Created by Atul Jain on 29/03/18.
//  Copyright © 2018 apporio. All rights reserved.
//

import UIKit

class AllTransactionViewController: UIViewController,UITableViewDelegate,UITableViewDataSource, MainCategoryProtocol {
    
    @IBOutlet weak var newtransactiontable: UITableView!
    
    var viewtransactiondata : ViewTransactionModel!
    
    var toastLabel : UILabel!
    
     var collectionsize = 0
    
    let Userid = NsUserDekfaultManager.SingeltionInstance.getuserdetaild(key: NsUserDekfaultManager.Keyuserid)


    override func viewDidLoad() {
        super.viewDidLoad()
        
        toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 150, y: self.view.frame.size.height-300, width: 300, height: 35))
        toastLabel.backgroundColor = UIColor.white
        toastLabel.textColor = UIColor.black
        toastLabel.textAlignment = NSTextAlignment.center;
        self.view.addSubview(toastLabel)
        toastLabel.text =  "No Transaction!!".localized
        
        toastLabel.isHidden = true

        
        ApiManager.sharedInstance.protocolmain_Catagory = self
        ApiManager.sharedInstance.WalletTransactionMehod(UserId: Userid!)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return collectionsize
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = newtransactiontable.dequeueReusableCell(withIdentifier: "transactioncell", for: indexPath)
        
        
        let mainview: UIView = (cell.contentView.viewWithTag(1) as? UIView)!
        
        let price: UILabel = (cell.contentView.viewWithTag(2) as? UILabel)!
        
        let amountcredit: UILabel = (cell.contentView.viewWithTag(3) as? UILabel)!
        
        let date: UILabel = (cell.contentView.viewWithTag(4) as? UILabel)!
        
         let image1 : UIImageView = (cell.contentView.viewWithTag(5) as? UIImageView)!
        
        
        mainview.layer.shadowColor = UIColor.gray.cgColor
        mainview.layer.shadowOpacity = 1
        mainview.layer.cornerRadius = 5
        mainview.layer.shadowOffset = CGSize(width:-0, height: 5)
        mainview.layer.shadowRadius = 5
        
       // icons8-down-arrow-48
        
        price.text = GlobalVarible.currencysymbol + " " + (viewtransactiondata.details?[indexPath.row].amount)!
        
        date.text = viewtransactiondata.details?[indexPath.row].date
        
        if viewtransactiondata.details?[indexPath.row].transfer == "1"{
            
             amountcredit.text = "Amount credited".localized
        image1.image = UIImage(named: "icons8-down-arrow-48")
        }else if viewtransactiondata.details?[indexPath.row].transfer == "2"{
             amountcredit.text = "Amount debited".localized
        image1.image = UIImage(named: "icons8-up-48")
        
        }else{
        
        }
        
              
       
        
  return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        newtransactiontable.deselectRow(at: indexPath, animated: true)
        let row = indexPath.row
        print("Row: \(row)")
        

        
    }
    
    
    func showalert(message:String)  {
        
        DispatchQueue.main.async(execute: {
            
            let alertController = UIAlertController(title:   "Alert".localized, message:message, preferredStyle: .alert)
            
            
            let OKAction = UIAlertAction(title: "ok".localized, style: .default) { (action) in
                
            }
            alertController.addAction(OKAction)
            
            self.present(alertController, animated: true) {
                
            }
            
            
        })
        
    }
    
    
    func onProgressStatus(value: Int) {
        if(value == 0 ){
            MBProgressHUD.hide(for: self.view, animated: true)
        }else if (value == 1){
            let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
            spinnerActivity.label.text = "Loading".localized
            spinnerActivity.detailsLabel.text = "Please Wait!!".localized
            spinnerActivity.isUserInteractionEnabled = false
            
        }
    }
    
    func onSuccessExecution(msg: String) {
        print("\(msg)")
    }
    
    
    func onerror(msg : String) {
        MBProgressHUD.hide(for: self.view, animated: true)
        
        self.showalert(message: msg)
        
    }
    
    
    func onSuccessParse(data: AnyObject) {
        
         if(GlobalVarible.Api == "wallettransaction"){
        
            if let viewtransactiondata = data as? ViewTransactionModel{
        self.viewtransactiondata = viewtransactiondata
        
        if(viewtransactiondata.status == 0){
            
           
            
            toastLabel.isHidden = false
            newtransactiontable.isHidden = true
            
            
        }else{
            
            toastLabel.isHidden = true
            newtransactiontable.isHidden = false
            
            collectionsize = (viewtransactiondata.details?.count)!
            
            newtransactiontable.reloadData()
            
        }

        }
        
        }
    }
    

   
}
