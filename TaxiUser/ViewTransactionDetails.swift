//
//  Details.swift
//
//  Created by Atul Jain on 29/03/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public class ViewTransactionDetails {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let walletTransactionId = "wallet_transaction_id"
    static let date = "date"
    static let amount = "amount"
    static let userId = "user_id"
    static let transfer = "transfer"
  }

  // MARK: Properties
  public var walletTransactionId: String?
  public var date: String?
  public var amount: String?
  public var userId: String?
  public var transfer: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    walletTransactionId = json[SerializationKeys.walletTransactionId].string
    date = json[SerializationKeys.date].string
    amount = json[SerializationKeys.amount].string
    userId = json[SerializationKeys.userId].string
    transfer = json[SerializationKeys.transfer].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = walletTransactionId { dictionary[SerializationKeys.walletTransactionId] = value }
    if let value = date { dictionary[SerializationKeys.date] = value }
    if let value = amount { dictionary[SerializationKeys.amount] = value }
    if let value = userId { dictionary[SerializationKeys.userId] = value }
    if let value = transfer { dictionary[SerializationKeys.transfer] = value }
    return dictionary
  }

}
