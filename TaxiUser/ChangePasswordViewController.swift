//
//  ChangePasswordViewController.swift
//  Apporio Taxi Driver
//
//  Created by AppOrio on 12/06/17.
//  Copyright © 2017 apporio. All rights reserved.
//

import UIKit

class ChangePasswordViewController: UIViewController,MainCategoryProtocol  {
    
     var changepasswordresponse : NewChangePassword!
    
    var oldPwd: String = ""
    var newPwd: String = ""
    var cnfrmPwd: String = ""
       var defaultdriverid = ""
    
    
    
   
    @IBOutlet weak var donebtn: UIButton!
    
    @IBOutlet weak var confirm_pwd_field: UITextField!
    @IBOutlet weak var new_pwd_field: UITextField!
    @IBOutlet weak var old_pwd_field: UITextField!
    
    @IBOutlet weak var changepasswordtextlabel: UILabel!

   let Userid = NsUserDekfaultManager.SingeltionInstance.getuserdetaild(key: NsUserDekfaultManager.Keyuserid)

    override func viewDidLoad() {
        super.viewDidLoad()
        setColor()
        changepasswordtextlabel.text = "Change Password".localized
        old_pwd_field.placeholder = "Old Password".localized
        new_pwd_field.placeholder = "New Password".localized
        confirm_pwd_field.placeholder = "Confirm Password".localized
        donebtn.setTitle("DONE".localized, for: UIControlState.normal)
        
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backbtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        
        
    }

    @IBAction func Donebtn(_ sender: Any) {
        
        oldPwd = old_pwd_field.text!
        newPwd = new_pwd_field.text!
        cnfrmPwd = confirm_pwd_field.text!
        
        if (newPwd == cnfrmPwd){
            
            
           
            let dic=[ changePassword2:"\(self.Userid!)",
                changePassword3:"\(oldPwd)",
                changePassword4:"\(newPwd)",
                changePassword5:"\(GlobalVarible.languagecode)"
            ]
            
            ApiManager.sharedInstance.protocolmain_Catagory = self
            ApiManager.sharedInstance.postData1(dictonary: dic as NSDictionary, url: changePassword)
            
           
        }else{
            
        self.showalert(message: "Password Does Not Match".localized)
        }

        
        
    }
    
   
    func setColor() {
        changepasswordtextlabel.textColor = AppColors.themeColor
        donebtn.setTitleColor(AppColors.whiteColor, for: .normal)
        donebtn.backgroundColor = AppColors.themeColor
    }
    
    
    func showalert(message:String)  {
        
        DispatchQueue.main.async(execute: {
            
            let alertController = UIAlertController(title:   "Alert".localized, message:message, preferredStyle: .alert)
            
            
            let OKAction = UIAlertAction(title: "ok".localized, style: .default) { (action) in
                
            }
            alertController.addAction(OKAction)
            
            self.present(alertController, animated: true) {
                
            }
            
            
        })
        
    }
    
    func showalert1(message:String)  {
        
        DispatchQueue.main.async(execute: {
            
            let alertController = UIAlertController(title:   "Alert".localized, message:message, preferredStyle: .alert)
            
            
            let OKAction = UIAlertAction(title: "ok".localized, style: .default) { (action) in
                self.dismiss(animated: true, completion: nil)
            }
            alertController.addAction(OKAction)
            
            self.present(alertController, animated: true) {
                
            }
            
            
        })
        
    }

    


    
    
    func onProgressStatus(value: Int) {
        if(value == 0 ){
            MBProgressHUD.hide(for: self.view, animated: true)
        }else if (value == 1){
            let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
            spinnerActivity.label.text = "Loading".localized
            spinnerActivity.detailsLabel.text = "Please Wait!!".localized
            spinnerActivity.isUserInteractionEnabled = false
            
        }
    }
    
    func onSuccessExecution(msg: String) {
        print("\(msg)")
    }
    
    
    func onerror(msg : String) {
        MBProgressHUD.hide(for: self.view, animated: true)
        
        self.showalert(message: msg)
        
    }
    func onSuccessParse(data: AnyObject) {
        
        if let changepasswordresponse = data as? NewChangePassword{
        
            self.changepasswordresponse = changepasswordresponse
        
        if(changepasswordresponse.result == 1){
            
            self.showalert1(message: changepasswordresponse.message!)
            
        }else{
            
            self.showalert(message: changepasswordresponse.message!)
            
        }
        
        }
        
    }
    
//    
//    func onSuccessState(_ data: AnyObject , resultCode: Int) {
//        
//        self.data = data as! ChangePassword
//        if(self.data.result == 1){
//            
//            let alert = UIAlertController(title: "", message: self.data.msg! , preferredStyle: .alert)
//            let action = UIAlertAction(title: "OK".localized, style: .default) { _ in
//                
//                
//            }
//            alert.addAction(action)
//            self.present(alert, animated: true){}
//        }
//        else{
//            
//            let alert = UIAlertController(title: "", message: self.data.msg! , preferredStyle: .alert)
//            let action = UIAlertAction(title: "OK".localized, style: .default) { _ in
//                
//            }
//            alert.addAction(action)
//            self.present(alert, animated: true){}
//        }
//        
//    }
//
//    
    
   

}
