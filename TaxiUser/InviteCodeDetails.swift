//
//  Details.swift
//
//  Created by Atul Jain on 23/03/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public class InviteCodeDetails {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let referralCode = "referral_code"
    static let userId = "user_id"
    static let endDate = "end_date"
    static let startDate = "start_date"
    static let offer = "offer"
    static let applicationurl = "application_url"
  }

  // MARK: Properties
  public var referralCode: String?
  public var userId: String?
  public var endDate: String?
  public var startDate: String?
  public var offer: String?
    public var applicationurl: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    referralCode = json[SerializationKeys.referralCode].string
    userId = json[SerializationKeys.userId].string
    endDate = json[SerializationKeys.endDate].string
    startDate = json[SerializationKeys.startDate].string
    offer = json[SerializationKeys.offer].string
    applicationurl = json[SerializationKeys.applicationurl].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = referralCode { dictionary[SerializationKeys.referralCode] = value }
    if let value = userId { dictionary[SerializationKeys.userId] = value }
    if let value = endDate { dictionary[SerializationKeys.endDate] = value }
    if let value = startDate { dictionary[SerializationKeys.startDate] = value }
    if let value = offer { dictionary[SerializationKeys.offer] = value }
     if let value = applicationurl { dictionary[SerializationKeys.applicationurl] = value }
    return dictionary
  }

}
