//
//  Response.swift
//
//  Created by Atul Jain on 13/06/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public class Response {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let holderData = "holder_data"
    static let holderName = "holder_name"
  }

  // MARK: Properties
  public var holderData: String?
  public var holderName: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    holderData = json[SerializationKeys.holderData].string
    holderName = json[SerializationKeys.holderName].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = holderData { dictionary[SerializationKeys.holderData] = value }
    if let value = holderName { dictionary[SerializationKeys.holderName] = value }
    return dictionary
  }

}
