//
//  FavoriteLocationAddVC.swift
//  Apporio Taxi
//
//  Created by Apporio on 09/03/18.
//  Copyright © 2018 apporio. All rights reserved.
//

import UIKit

class FavoriteLocationAddVC: UIViewController, MainCategoryProtocol {

    //MARK: - Outlets
    
    @IBOutlet weak var viewFav: UIView!
    @IBOutlet weak var txtLocationName: UITextField!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var segmentMenu: UISegmentedControl!
    @IBOutlet weak var otherNameFieldConst: NSLayoutConstraint!
    
    var getIndex = "1"
    var strAddress: String?
    var dataFav: Coupons!
    
   
    @IBOutlet weak var savebtntext: UIButton!
    
    let Userid = NsUserDekfaultManager.SingeltionInstance.getuserdetaild(key: NsUserDekfaultManager.Keyuserid)

    //MARK: - ViewSetup
    
    func ViewSetup(){
        viewFav.layer.cornerRadius = 5.0
        //viewFav.layer.borderColor = Uic
        viewFav.layer.borderWidth = 1.0
        viewFav.edgeWithShadow()
        
    }
    
    
    //MARK: - Initialization
    
    func Initialization(){
        otherNameFieldConst.constant = 0
        
        
    }
    
    //MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblAddress.text = "Your address".localized
        txtLocationName.placeholder = "Enter Name".localized
        savebtntext.setTitle("Save".localized, for: UIControlState.normal)
        btnCancel.setTitle("Cancel".localized, for: UIControlState.normal)
        ViewSetup()
        Initialization()
        lblAddress.text = strAddress
        
        
        
    }

    
    //MARK: - Action
    
    @IBAction func btnCancel(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    
    }
    
    
    @IBAction func btnSave(_ sender: Any) {
        
        let CategoryValue = segmentMenu.selectedSegmentIndex + 1
        let CategoryStringValue = String(format: "%d", CategoryValue)
        
        if CategoryStringValue == "3"{
            
            if txtLocationName.text?.count == 0 {
                
                self.showalert(message: "Please Enter Name For This Location", ValueCheck: "1")
                return
            }
            
        }
        
        
        if getIndex == "1" {
            
            ApiManager.sharedInstance.protocolmain_Catagory = self
            ApiManager.sharedInstance.AddLocationCategory(UserId: Userid!, Lat: GlobalVarible.PickUpLat, Long: GlobalVarible.PickUpLng, Location: strAddress!, Category: CategoryStringValue, OtherName:txtLocationName.text!)
            
            
        } else {
            
            
            let userDropLat = String(format:"%.4f", GlobalVarible.UserDropLat)
            let userDropLong = String(format:"%.4f", GlobalVarible.UserDropLng)
            
            ApiManager.sharedInstance.protocolmain_Catagory = self
            ApiManager.sharedInstance.AddLocationCategory(UserId: Userid!, Lat: userDropLat, Long: userDropLong, Location: strAddress!, Category: CategoryStringValue, OtherName: txtLocationName.text!)
            
            
            
        }
        
    }
    
    
    @IBAction func segmentControll(_ sender: Any) {
    
        switch segmentMenu.selectedSegmentIndex
        {
        case 0:
                    self.otherNameFieldConst.constant = 0
            print("Segment 0 selected")
            
        case 1:
                    self.otherNameFieldConst.constant = 0
            print("Segment 1 selected")
            
        case 2:
                    self.otherNameFieldConst.constant = 30
            print("Segment 2 selected")
            
        default:
            break;
        }
        
    }
    
    
    
    
    //MARK: -  Protocols
    
    
    func onProgressStatus(value: Int) {
        if(value == 0 ){
            MBProgressHUD.hide(for: self.view, animated: true)
        }else if (value == 1){
            let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
            spinnerActivity.label.text = "Loading".localized
            spinnerActivity.detailsLabel.text = "Please Wait!!".localized
            spinnerActivity.isUserInteractionEnabled = false
            
        }
    }
    
    func onSuccessExecution(msg: String) {
        print("\(msg)")
    }
    
    
    func onerror(msg : String) {
        MBProgressHUD.hide(for: self.view, animated: true)
        
        self.showalert(message: msg, ValueCheck: "1")
        
    }
    
    
    func onSuccessParse(data: AnyObject) {
        
        if(GlobalVarible.Api == "AddFavorite"){
            
            
            if let  dataFav = data as? Coupons{
            self.dataFav = dataFav
            if dataFav.result == 1{
                
                showalert(message: dataFav.msg!, ValueCheck: "2")
                
            } else {
                
                showalert(message: dataFav.msg!, ValueCheck: "1")
                
                
                
            }
            
            }
        }
    }
    
    
    
    func showalert(message:String, ValueCheck:String)  {
        
        DispatchQueue.main.async(execute: {
            
            let alertController = UIAlertController(title: "Alert".localized, message:message, preferredStyle: .alert)
            
            
            let OKAction = UIAlertAction(title: "ok".localized, style: .default) { (action) in
                
                if ValueCheck == "2"{
                
                    self.dismiss(animated: false, completion: nil)
                
                }
                
                
                
            }
            alertController.addAction(OKAction)
            
            self.present(alertController, animated: true) {
                
            }
            
            
        })
        
    }

    
}
