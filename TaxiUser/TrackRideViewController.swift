//
//  TrackRideViewController.swift
//  TaxiUser
//
//  Created by AppOrio on 24/05/17.
//  Copyright © 2017 apporio. All rights reserved.
//

import UIKit
import GoogleMaps
import AlamofireImage
import SwiftyJSON
import Alamofire
import StarryStars
import MapKit
import CoreLocation
import Firebase
import GooglePlaces
import JSQMessagesViewController

var trackrideviewcontroller : TrackRideViewController!


class TrackRideViewController: UIViewController,CLLocationManagerDelegate,GMSMapViewDelegate,MainCategoryProtocol,GMSAutocompleteViewControllerDelegate {
    
    var messages = [JSQMessage]()
    
    var part1: String = ""
    var part2: String = ""
    var part3: String = ""
    
     var changedata: DropChange!
    
     var driverlatlngdata: DriverlatlngModel!
    
    var ridesharedata: RideShareModel!
    
    
    @IBOutlet weak var Clicktoviewtextlabel: UILabel!
    @IBOutlet weak var chatviewhidden: UIView!
    
    @IBOutlet weak var Chatnewmessage: UILabel!
    
    @IBOutlet weak var Newmessagetextlabel: UILabel!
    
  //  var driverdata: DriverInfo!
   // var trackdata: DriveTrackModel!
   // var Doneridedata : DoneRideModel!
    
  //  var drivertrackdata : DriveTrackModel!
    
   // @IBOutlet weak var drivernameview: UIView!
    
    @IBOutlet weak var cancelbuttonview: UIView!
    
    @IBOutlet weak var driverratingresult: UILabel!
    
    @IBOutlet weak var sosbtn: UIButton!
    
    
    let imageUrl = API_URL.imagedomain
    
    var timer = Timer()
    
    var totaldistance = 3000
    
    var markerList = [GMSMarker]()
    
    var driverid = ""
    
    var timerForGetDriverLocation = Timer()
    
    @IBOutlet weak var cancelbutton: UIButton!
    
    var radiansBearing : Double = 0.0
    
     var checkstarttimervalue = 1
    var originMarker: GMSMarker!
    var destinationMarker: GMSMarker!
 //   var routePolyline: GMSPolyline!
    
    var Currentrideid = ""
    
    var currentStatus = ""
    var currentmessage = ""
    var startlat = ""
    var startlng = ""
    var destinationlat = ""
    var destinationlng = ""
    
    var driverstandlat = "0.0"
     var driverstandlong = "0.0"
    
    var temparorylat = ""
    var temparorylong = ""
    
    var marker = GMSMarker()
    
    var temparoryvalue = 1
    
    var temparoryvalue1 = 1
    
    var markervalue = 1
    
    var originCoordinate: CLLocationCoordinate2D!
    
    var destinationCoordinate: CLLocationCoordinate2D!
    
    
    @IBOutlet weak var calldrivertextlabel: UILabel!
    
    
    @IBOutlet weak var shareridelinktextlabel: UILabel!

    
    @IBOutlet weak var chatwithdrivertextlabel: UILabel!
    
    var polyline = GMSPolyline()
    var animationPolyline = GMSPolyline()
    var path = GMSPath()
    var animationPath = GMSMutablePath()
    var i: UInt = 0
    var timer1: Timer!
    
    var ref = FIRDatabase.database().reference()
    
    var ref1 = FIRDatabase.database().reference()
    
    var Lat = ""
    var Lng = ""
    
    
    var x = 0
    var k = 0
    
    var DRIVERLAT = " "
    var DRIVERLNG = " "
    var PickupLoc = ""
    var Drivercurrentaddress = ""
    var Driverdroplocation = ""
    
    var mydatapage: DriverInfo!
    let locationManager = CLLocationManager()
    
    private let googleMapsKey = "AIzaSyAwdw2gOgLTM_lAjEtVvIH87xHx3RTKEUQ"
    private let baseURLString = "https://maps.googleapis.com/maps/api/directions/json?"
    
      let baseURLDirections = "https://maps.googleapis.com/maps/api/directions/json?"
    
     var dropchange = 0
    
    @IBOutlet weak var ridestatustoplabel: UILabel!
    
  //  @IBOutlet weak var ratingview: UIView!
    
    @IBOutlet weak var driverimageview: UIImageView!
    
    @IBOutlet weak var drivername: UILabel!
    
    @IBOutlet weak var carimagecircleview: UIView!
    
    @IBOutlet weak var carimage: UIImageView!
    
    @IBOutlet weak var carname: UILabel!
    
    @IBOutlet weak var carnumber: UILabel!
    
    @IBOutlet weak var carmodelname: UILabel!
    
    //  var newtimer: DispatchSourceTimer?
    
    
    @IBOutlet weak var greenmarkerlocation: UILabel!
    
    @IBOutlet weak var redmarkerlocation: UILabel!
    
    @IBOutlet weak var mapview: GMSMapView!
    

 
    override func viewDidLoad() {
        super.viewDidLoad()
        
       // cancelbutton.layer.borderWidth = 1.0
     //   cancelbutton.layer.backgroundColor = (UIColor(red:147.0/255.0, green:165.0/255.0, blue:165.0/255.0, alpha:1.0) as! CGColor)

      //  cancelbutton.layer.cornerRadius = 4
        
        
        if(GlobalVarible.cartypeimage == " "){
            
            
            carimage.image = UIImage(named: "car@100") as UIImage?
            
            //
            //carnametext.text = "null"
        }else{
            let newUrl = imageUrl + GlobalVarible.cartypeimage
            //    let url = "http://apporio.co.uk/apporiotaxi/\(GlobalVarible.cartypeimage)"
            
            let url1 = NSURL(string: newUrl)
            carimage!.af_setImage(withURL:
                url1! as URL,
                                  placeholderImage: UIImage(named: "dress"),
                                  filter: nil,
                                  imageTransition: .crossDissolve(1.0))
            
        }
        
        

        
         self.chatviewhidden.isHidden = true
        
      calldrivertextlabel.text = "Call Driver".localized
        shareridelinktextlabel.text = "Share Ride Link".localized
        chatwithdrivertextlabel.text = "Chat with Driver".localized
        Newmessagetextlabel.text = "New Message".localized
        Clicktoviewtextlabel.text = "Click to View".localized
        cancelbutton.setTitle("Cancel Ride".localized, for: UIControlState.normal)

        
      /*  if(GlobalVarible.changeddestination == 1){
            
            ApiManager.sharedInstance.protocolmain_Catagory = self
            ApiManager.sharedInstance.DriverInformation(RIDEID: GlobalVarible.checkRideId)
            
            
            let Message: NSDictionary = ["changed_destination": "0","ride_id": GlobalVarible.checkRideId,"ride_status": GlobalVarible.checkridestatus,"done_ride_id": ""]
            
            self.ref.child("RideTable").child(GlobalVarible.checkRideId).setValue(Message)
            
            GlobalVarible.changeddestination = 0
            
        }*/
        
        //self.changelocationtimer()
        
        
        
        let query = self.ref.child("Chat").child(GlobalVarible.checkRideId)
        // let query = ref1.child("RideTable").child(GlobalVariables.rideid).child("Chat").queryLimited(toLast: 10)
        
        print(query)
        
        
        _ = query.observe(.childAdded, with: { [weak self] snapshot in
            
            
            
            if  let data        = snapshot.value as? [String: String],
                let id          = data["send_via"],
                let name        = data["timestamp"],
                let text        = data["message"],
                !text.isEmpty   // <-- check if the text length > 0
            {
                // Create a new JSQMessage object with the ID, display name and text
                if let message = JSQMessage(senderId: id, displayName: name, text: text)
                {
                    // Append to the local messages array
                    self?.messages.append(message)
                    
                    
                    self?.chatviewhidden.isHidden = false
                    
                    self?.Chatnewmessage.text = text
                    
                    self?.callchatview()
                    
                    // Tell JSQMVC that we're done adding this message and that it should reload the view
                    //self?.finishReceivingMessage()
                }
            }
        })
        

        
        
      //  drivernameview.layer.borderWidth = 1.0
      //  drivernameview.layer.cornerRadius = 4
        
        print(GlobalVarible.checkRideId)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(showtrack),
            name: NSNotification.Name(rawValue: "trackride"),
            object: nil)
        
      
        
        trackrideviewcontroller = self
        dropchange = 0
        
        
       /* carimagecircleview.layer.borderWidth = 1
        carimagecircleview.layer.masksToBounds = false
        carimagecircleview.layer.borderColor = UIColor.black.cgColor
        carimagecircleview.layer.cornerRadius =  carimagecircleview.frame.height/2
        carimagecircleview.clipsToBounds = true*/
        
        GlobalVarible.rideendstopupdatelocation = 0
        
     /*   ratingview.layer.borderWidth = 1
        ratingview.layer.masksToBounds = false
        ratingview.layer.borderColor = UIColor.black.cgColor
        ratingview.layer.cornerRadius =  carimagecircleview.frame.height/2
        ratingview.clipsToBounds = true*/
        
        
        driverimageview.layer.borderWidth = 1
        driverimageview.layer.masksToBounds = false
        driverimageview.layer.borderColor = UIColor.black.cgColor
        driverimageview.layer.cornerRadius =  driverimageview.frame.height/2
        driverimageview.clipsToBounds = true
        
        mapview.isUserInteractionEnabled =  true
        
        // mapview.myLocationEnabled = true
        
    //    GlobalSelcetvaluue = 1
        
      
        do {
            // Set the map style by passing the URL of the local file.
            if let styleURL = Bundle.main.url(forResource: "uber_theme", withExtension: "json") {
                mapview.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                NSLog("Unable to find style.json")
            }
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
        
        

        
        ApiManager.sharedInstance.protocolmain_Catagory = self
        ApiManager.sharedInstance.DriverInformation(RIDEID: GlobalVarible.checkRideId)
        
        //  self.starttimer()
        
        GlobalVarible.StringMatchPayment = "hellohi"
        
        mapview.delegate = self
        
        


        // Do any additional setup after loading the view.
    }
    
    func callchatview(){
        
        let when = DispatchTime.now() + 4 // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            
            self.chatviewhidden.isHidden = true
            
            // Your code with delay
        }
        
    }
    
    func createTimer() {
        
       GlobalVarible.newtimer = DispatchSource.makeTimerSource(queue: .main)
        
        
        GlobalVarible.newtimer?.scheduleRepeating(deadline: .now(), interval: .seconds(5))
        
        
        GlobalVarible.newtimer?.setEventHandler(handler: { [weak self] in      // assuming you're referencing `self` in here, use `weak` to avoid strong reference cycles
            // do something
            
            ApiManager.sharedInstance.protocolmain_Catagory = self
            ApiManager.sharedInstance.getdriverlatlngmethod(DriverId: (self?.driverid)!)
            
            
            //  print("timerstatrt")
        })
        GlobalVarible.newtimer?.resume()
        // note, timer is not yet started; you have to call `timer?.resume()`
    }
    
    func starttimer() {
        // timer?.resume()
        createTimer()
    }
    
    
    func stopTimer() {
        GlobalVarible.newtimer?.cancel()
        GlobalVarible.newtimer = nil
    }
    


    
    
    @IBAction func chatviewbtn_click(_ sender: Any) {
        let storyboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let LoginView = storyboard.instantiateViewController(withIdentifier: "ChatViewController")
        let navController = UINavigationController(rootViewController: LoginView)
        self.present(navController, animated: true, completion: nil)

        
    }
    
    
    
    
    func showtrack(notification: NSNotification){
        
       // ApiManager.sharedInstance.protocolmain_Catagory = self
      //  ApiManager.sharedInstance.DriverInformation(RIDEID: Currentrideid)
        
        
        if(GlobalVarible.changeddestination == 1){
            
         GlobalVarible.changeddestination = 0
            ApiManager.sharedInstance.protocolmain_Catagory = self
              ApiManager.sharedInstance.DriverInformation(RIDEID: GlobalVarible.checkRideId)
            
            
           
            
           

        
        }
        
        
        
        if(GlobalVarible.checkridestatus == "3"){
            
            print(DRIVERLAT)
            print(DRIVERLNG)
            
            currentStatus = GlobalVarible.checkridestatus
            
             UserDefaults.standard.setValue(GlobalVarible.checkridestatus, forKey:"firebaseride_status")
            //  let origin = GlobalVarible.PickUpLat + "," + GlobalVarible.PickUpLng
            //   let destination = DRIVERLAT + "," + DRIVERLNG
            
            //   self.getDirections(origin: origin, destination: destination, waypoints: nil, travelMode: nil)
              self.checkstarttimervalue = 1
            self.starttimer()
            ridestatustoplabel.text = "Arriving Now".localized
            
            sosbtn.isHidden = true
            
        }
        
        if(GlobalVarible.checkridestatus == "5"){
            
            k = 0
            // self.Timer.invalidate()
            currentStatus = GlobalVarible.checkridestatus
            self.stopTimer()
            self.mapview.clear()
            
            UserDefaults.standard.setValue(GlobalVarible.checkridestatus, forKey:"firebaseride_status")
            
           
            
            cancelbutton.isHidden = true
            cancelbuttonview.isHidden = true
            sosbtn.isHidden = true
            x = 1
            startlat = GlobalVarible.PickUpLat
            startlng = GlobalVarible.PickUpLng
            
            ridestatustoplabel.text = "Ride Arrived".localized
            destinationlat = String(GlobalVarible.UserDropLat)
            destinationlng = String(GlobalVarible.UserDropLng)
            
            self.drawRoute1()
            
//            let origin = startlat + "," + startlng
//            let destination = destinationlat + "," + destinationlng
//
//            self.getDirections(origin: origin, destination: destination, waypoints: nil, travelMode: nil)
            
            //   self.createRoute()
            
            // showTextToast(currentmessage)
            //   self.showalert1(currentmessage)
            
            
        }
        
        if(GlobalVarible.checkridestatus == "6"){
            
            k = 0
            //  self.Timer.invalidate()
            
           
             self.mapview.clear()
            
            currentStatus = GlobalVarible.checkridestatus
            
            UserDefaults.standard.setValue(GlobalVarible.checkridestatus, forKey:"firebaseride_status")
            
            self.temparoryvalue = 1
            
            startlat = GlobalVarible.PickUpLat
            startlng = GlobalVarible.PickUpLng
            
           // GlobalVarible.UserDropLat =  Double(mydatapage.details!.dropLat!)!
            //GlobalVarible.UserDropLng = Double(mydatapage.details!.dropLong!)!
            
            
            destinationlat = String(GlobalVarible.UserDropLat)
            destinationlng =  String(GlobalVarible.UserDropLng)
            
            
            ridestatustoplabel.text = "Riding Now".localized
            
            Driverdroplocation = (mydatapage.details!.dropLocation)!
            
            //   self.showalert1(currentmessage)
            
            self.checkstarttimervalue = 2
            self.starttimer()
            
            x = 1
            cancelbutton.isHidden = true
            cancelbuttonview.isHidden = true
            sosbtn.isHidden = false
            self.mapview.clear()
            
            
            
        }
        
        
        if(GlobalVarible.checkridestatus == "7"){
            
             self.stopTimer()
            UserDefaults.standard.setValue(GlobalVarible.checkridestatus, forKey:"firebaseride_status")
            
            ApiManager.sharedInstance.protocolmain_Catagory = self
            ApiManager.sharedInstance.DriverInformation(RIDEID: GlobalVarible.checkRideId)
            
          /*  let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextController: RecieptViewController = storyboard.instantiateViewController(withIdentifier: "RecieptViewController") as! RecieptViewController
            
            nextController.currentrideid = mydatapage.details!.doneRideId!
            
            
            self.present(nextController, animated: true, completion: nil)*/
            
            
        }
            
            
        else{
            
            
        }


        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func chatbtn_click(_ sender: Any) {
        
        
        let storyboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let LoginView = storyboard.instantiateViewController(withIdentifier: "ChatViewController")
        let navController = UINavigationController(rootViewController: LoginView)
        self.present(navController, animated: true, completion: nil)
        

        
        
    }
    
    
    
    @IBAction func sosbtnclick(_ sender: Any) {
        
        
        
        let refreshAlert = UIAlertController(title:  "EMERGENCY FOR CALL".localized, message: "Are You in problem ?".localized, preferredStyle: UIAlertControllerStyle.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Yes".localized , style: .default, handler: { (action: UIAlertAction!) in
            
            let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let next: EmergencyViewController = storyboard.instantiateViewController(withIdentifier: "EmergencyViewController") as! EmergencyViewController
            self.present(next, animated: true, completion: nil)
            
        }))
        
        
        refreshAlert.addAction(UIAlertAction(title: "No".localized, style: .default, handler: { (action: UIAlertAction!) in
            
            refreshAlert .dismiss(animated: true, completion: nil)
            
            
        }))
        
        present(refreshAlert, animated: true, completion: nil)
        

        
        
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
       // self.timer1.invalidate()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // self.timer1.invalidate()
        if self.dropchange == 1{
            ApiManager.sharedInstance.protocolmain_Catagory = self
            ApiManager.sharedInstance.changeDrop(droplat: String(GlobalVarible.UserDropLat), droplong: String(GlobalVarible.UserDropLng), droplocation: GlobalVarible.UserDropLocationText, rideid: GlobalVarible.checkRideId)
        }
    }

    
   @IBAction func edit_drop_location_click(_ sender: Any) {
        
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
    
    
    
    
    @IBAction func backbtn(_ sender: Any) {
        
        GlobalVarible.trackbackbtnvaluematch = 1
        
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextController: MapViewController = storyboard.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        
        if let window = self.view.window{
            window.rootViewController = nextController
        }

        
     
        
        
    }
    
    @IBAction func cancelbtn(_ sender: Any) {
        
        GlobalVarible.newtimer?.cancel()
        GlobalVarible.newtimer = nil
        stopTimer()
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let next: ReasonDialogController = storyboard.instantiateViewController(withIdentifier: "ReasonDialogController") as! ReasonDialogController
        next.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        next.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        next.movefrom = ""
        self.present(next, animated: true, completion: nil)

        
        
    }
    
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedAlways{
            
            locationManager.startUpdatingLocation()
           
           
        }
    }
    
    func locationManager(_ manager: CLLocationManager,didUpdateLocations locations: [CLLocation]) {
        
        if let location = locations.first {
            
            if GlobalVarible.rideendstopupdatelocation == 0
            {
                
            }else{
                
                self.locationManager.stopUpdatingLocation()
            }
            
            print(location)
            
         //  reverseGeocodeCoordinate(coordinate: location.coordinate)
            
        }
        
        
    }
    
    
    
    
    


    
    func cresterootviatrack(lat: String , long: String ,BearningFactor : String){
        
        // func cresterootviatrack(lat: String , long: String){
        
        
        let coordinates = CLLocationCoordinate2D(latitude: Double(lat)!, longitude: Double(long)!)
        
        if markervalue == 1 {
            marker = GMSMarker(position: coordinates)
           marker.icon = UIImage(named: "driver_icon")
            
            markervalue = 0
        }
        
        CATransaction.begin()
        CATransaction.setAnimationDuration(1.0)
        marker.rotation = Double(BearningFactor)!
        CATransaction.commit()
        
        CATransaction.begin()
        CATransaction.setAnimationDuration(1.0)
        marker.position =  coordinates
        CATransaction.commit()
        marker.map = mapview
        
        
        
    }
    
    func timercallapiresponse(){
        
        if(self.checkstarttimervalue == 1){
            
            driverstandlat = (driverlatlngdata.details?.currentLat)!
            
            driverstandlong = (driverlatlngdata.details?.currentLong)!
            
            
            let latitude = driverlatlngdata.details?.currentLat
            
            
            
            let longitude = driverlatlngdata.details?.currentLong
            
            
            let bearningdegree = driverlatlngdata.details?.bearingFactor
            
            print(latitude!)
            print(bearningdegree!)
            
            self.startlat = latitude!
            self.startlng = longitude!
            
            
            self.destinationlat = GlobalVarible.PickUpLat
            self.destinationlng = GlobalVarible.PickUpLng
            
        
            
            
          
                // self.createRoute()
                self.mapview.clear()
                //self.getDirections(origin: origin, destination: destination, waypoints: nil, travelMode: nil)
                self.drawRoute1()
                self.temparorylat = latitude!
                self.temparorylong = longitude!
                self.temparoryvalue = 0
                
          
        }else{
            
            
            let latitude1 =   driverlatlngdata.details?.currentLat
            
            
            
            let longitude1 = driverlatlngdata.details?.currentLong
            
            
            GlobalVarible.emergencylatitude = (driverlatlngdata.details?.currentLat)!
            
            
            GlobalVarible.emergencylongitude =  (driverlatlngdata.details?.currentLong)!
            
            
            
            
            let bearningdegree = driverlatlngdata.details?.bearingFactor
            
            
            print(latitude1!)
            print(bearningdegree!)
            
            
            
            let latitude = latitude1
            
            let longitude = longitude1
            
            // let bearningdegree = String(bearningdegree)
            
            self.startlat = latitude1!
            self.startlng = longitude1!
            
            self.destinationlat = String(GlobalVarible.UserDropLat)
            self.destinationlng = String(GlobalVarible.UserDropLng)
            
         
            
            
            
           
                //  self.createRoute()
                self.mapview.clear()
              //  self.getDirections(origin: origin, destination: destination, waypoints: nil, travelMode: nil)
                self.drawRoute1()
                self.temparorylat = latitude!
                self.temparorylong = longitude!
                self.temparoryvalue1 = 0
                
           
            
            
            
        }
    }
    
    
    
    
    

    

    
    
//    func getDirections(origin: String!, destination: String!, waypoints: Array<String>!, travelMode: AnyObject!){
//        if let originLocation = origin {
//            if let destinationLocation = destination {
//                let directionsURLString = baseURLDirections + "origin=" + originLocation + "&destination=" + destinationLocation + "&key=" + GlobalVarible.googlemapkeys
//
//
//                print(directionsURLString)
//
//                //  directionsURLString = directionsURLString.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!
//
//                let directionsURL = NSURL(string: directionsURLString)
//
//                DispatchQueue.main.async(execute: { () -> Void in
//
//                    let directionsData = NSData(contentsOf: directionsURL! as URL)
//
//
//
//                    do {
//
//                        let dictonary:Dictionary<String, Any> = try JSONSerialization.jsonObject(with: directionsData! as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as! Dictionary<String, Any>
//
//
//                        let status = dictonary["status"] as! String
//
//                        let json = JSON(data: directionsData! as Data)
//
//
//
//                        if status == "OK" {
//
//                              self.totaldistance = json["routes"][0]["legs"][0]["distance"]["value"].int!
//
//                            print(self.totaldistance)
//
//                        }
//
//
//                        self.drawRoute(routeDict: dictonary)
//
//                    } catch {
//                        NSLog("One or more of the map styles failed to load. \(error)")
//                    }
//
//
//                })
//            }
//            else {
//                //  completionHandler("Destination is nil.", false)
//            }
//        }
//        else {
//            // completionHandler("Origin is nil", false)
//        }
//
//
//    }
//
    
    
    func drawRoute1(){
        markerList.removeAll()
        
        var bounds = GMSCoordinateBounds()
        
        
        
        self.originCoordinate = CLLocationCoordinate2DMake(Double(self.startlat)!, Double(self.startlng)!)
        
        originMarker = GMSMarker(position: self.originCoordinate)
        
        
        
        self.markerList.append(originMarker)
        
        if(currentStatus == "3"){
            
            originMarker.icon = UIImage(named: "driver_icon")
            //  originMarker.icon = GMSMarker.markerImage(with: UIColor.red)
        }else if(currentStatus == "6"){
            
            originMarker.icon = UIImage(named: "driver_icon")
            
            
        }else{
            originMarker.icon = UIImage(named: "pickup")
        }
        originMarker.map = self.mapview
        
        
        self.destinationCoordinate = CLLocationCoordinate2DMake(Double(self.destinationlat)! , Double(self.destinationlng)!)
        
        destinationMarker = GMSMarker(position: self.destinationCoordinate)
        
        
        self.markerList.append(destinationMarker)
        
        
        if(currentStatus == "3"){
            destinationMarker.icon = UIImage(named: "pick_point")
            // destinationMarker.icon = GMSMarker.markerImage(with: UIColor.green)
            
        }else{
            destinationMarker.icon = UIImage(named: "droppoint")
        }
        
        destinationMarker.map = self.mapview
        
        
        for marker in markerList {
            bounds = bounds.includingCoordinate(marker.position)
        }
        
        
        
        if(currentStatus == "3"){
            
            let coordinateTo = CLLocation(latitude: Double(driverstandlat)!, longitude: Double(driverstandlong)!)
            
            
            
            let pickuplat = Double(GlobalVarible.PickUpLat)
            let pickuplng = Double(GlobalVarible.PickUpLng)
            
            let coordinateFrom = CLLocation(latitude: pickuplat! , longitude: pickuplng!)
            
            let distanceInMeter =  coordinateFrom.distance(from: coordinateTo)
            
            print(distanceInMeter)
            
            if distanceInMeter < 500{
                
                let update = GMSCameraUpdate.fit(bounds, with: UIEdgeInsetsMake(50, 60, 50, 60))
                mapview.animate(with: update)
                
                mapview.animate(toZoom: 15)
            }else{
                let update = GMSCameraUpdate.fit(bounds, with: UIEdgeInsetsMake(50, 60, 50, 60))
                mapview.animate(with: update)
                
            }
            
        }else{
            
            let update = GMSCameraUpdate.fit(bounds, with: UIEdgeInsetsMake(50, 60, 50, 60))
            mapview.animate(with: update)
        }
        if(currentStatus == "5"){
            
            //  self.timer1 = Timer.scheduledTimer(timeInterval: 0.080, target: self, selector: #selector(animatePolylinePath), userInfo: nil, repeats: true)
            
        }
        
    }
    
//    func drawRoute(routeDict: Dictionary<String, Any>) {
//
//        let routesArray = routeDict["routes"] as! NSArray
//
//
//
//
//
//
//        markerList.removeAll()
//
//        var bounds = GMSCoordinateBounds()
//
//        if (routesArray.count > 0)
//        {
//            let routeDict = routesArray[0] as! Dictionary<String, Any>
//            let routeOverviewPolyline = routeDict["overview_polyline"] as! Dictionary<String, Any>
//            let points = routeOverviewPolyline["points"]
//
//
//            // let legs = routeDict["legs"] as! Dictionary<String, Any>
//            //  let startLocationDictionary = routeDict["legs"][0]["start_location"] as! Dictionary<String, Any>
//
//            self.originCoordinate = CLLocationCoordinate2DMake(Double(self.startlat)!, Double(self.startlng)!)
//
//            originMarker = GMSMarker(position: self.originCoordinate)
//
//
//
//            self.markerList.append(originMarker)
//
//          if(currentStatus == "3"){
//
//             originMarker.icon = UIImage(named: "driver_icon")
//          //  originMarker.icon = GMSMarker.markerImage(with: UIColor.red)
//          }else if(currentStatus == "6"){
//
//          originMarker.icon = UIImage(named: "driver_icon")
//
//
//          }else{
//           originMarker.icon = UIImage(named: "pickup")
//            }
//           originMarker.map = self.mapview
//
//
//            self.destinationCoordinate = CLLocationCoordinate2DMake(Double(self.destinationlat)! , Double(self.destinationlng)!)
//
//            destinationMarker = GMSMarker(position: self.destinationCoordinate)
//
//
//            self.markerList.append(destinationMarker)
//
//
//             if(currentStatus == "3"){
//                 destinationMarker.icon = UIImage(named: "pick_point")
//           // destinationMarker.icon = GMSMarker.markerImage(with: UIColor.green)
//
//             }else{
//                destinationMarker.icon = UIImage(named: "droppoint")
//            }
//
//            destinationMarker.map = self.mapview
//
//
//            for marker in markerList {
//                bounds = bounds.includingCoordinate(marker.position)
//            }
//
//            self.path = GMSPath.init(fromEncodedPath: points as! String)!
//
//            self.polyline.path = path
//          //  self.polyline.strokeColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
//             self.polyline.strokeColor = UIColor.black
//            self.polyline.strokeWidth = 3.0
//            self.polyline.map = self.mapview
//
//            if(currentStatus == "3"){
//
//                let coordinateTo = CLLocation(latitude: Double(driverstandlat)!, longitude: Double(driverstandlong)!)
//
//
//
//                let pickuplat = Double(GlobalVarible.PickUpLat)
//                let pickuplng = Double(GlobalVarible.PickUpLng)
//
//                let coordinateFrom = CLLocation(latitude: pickuplat! , longitude: pickuplng!)
//
//                let distanceInMeter =  coordinateFrom.distance(from: coordinateTo)
//
//                print(distanceInMeter)
//
//                if distanceInMeter < 500{
//
//                    let update = GMSCameraUpdate.fit(bounds, with: UIEdgeInsetsMake(50, 60, 50, 60))
//                    mapview.animate(with: update)
//
//                    mapview.animate(toZoom: 15)
//                }else{
//                    let update = GMSCameraUpdate.fit(bounds, with: UIEdgeInsetsMake(50, 60, 50, 60))
//                    mapview.animate(with: update)
//
//                }
//
//            }else{
//
//            let update = GMSCameraUpdate.fit(bounds, with: UIEdgeInsetsMake(50, 60, 50, 60))
//            mapview.animate(with: update)
//            }
//            if(currentStatus == "5"){
//
//          //  self.timer1 = Timer.scheduledTimer(timeInterval: 0.080, target: self, selector: #selector(animatePolylinePath), userInfo: nil, repeats: true)
//
//            }
//        }
//    }
    
    func animatePolylinePath() {
        if (self.i < self.path.count()) {
            self.animationPath.add(self.path.coordinate(at: self.i))
            self.animationPolyline.path = self.animationPath
            self.animationPolyline.strokeColor = UIColor.black
            self.animationPolyline.strokeWidth = 3
            self.animationPolyline.map = self.mapview
            self.i += 1
        }
        else {
            self.i = 0
            self.animationPath = GMSMutablePath()
            self.animationPolyline.map = nil
        }
    }

    
    
    
    @IBAction func drivercallbtn(_ sender: Any) {
        
        if let url = URL(string: "telprompt://\( GlobalVarible.driverphonenumber)") {
            UIApplication.shared.open(url, completionHandler: { (success) in
                print("Settings opened: \(success)") // Prints true
            })
            
         
        }

        
    }
    
    @IBAction func sharedetailsbtn(_ sender: Any) {
        
        
        ApiManager.sharedInstance.protocolmain_Catagory = self
        ApiManager.sharedInstance.RideShareDetails(RideId: GlobalVarible.RideId)
        
        
    }
    
    
    
    func showalert(message:String)  {
        
        DispatchQueue.main.async(execute: {
            
            let alertController = UIAlertController(title:   "Alert".localized, message:message, preferredStyle: .alert)
            
            
            let OKAction = UIAlertAction(title: "ok".localized, style: .default) { (action) in
                
            }
            alertController.addAction(OKAction)
            
            self.present(alertController, animated: true) {
                
            }
            
            
        })
        
    }
    
    
    func onProgressStatus(value: Int) {
        if(value == 0 ){
            MBProgressHUD.hide(for: self.view, animated: true)
        }else if (value == 1){
            let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
            spinnerActivity.label.text = "Loading".localized
            spinnerActivity.detailsLabel.text = "Please Wait!!".localized
            spinnerActivity.isUserInteractionEnabled = false
            
        }
    }
    
    func onSuccessExecution(msg: String) {
        print("\(msg)")
    }
    
    
    func onerror(msg : String) {
        MBProgressHUD.hide(for: self.view, animated: true)
        
        self.showalert(message: msg)
        
    }
    
    
    
    func onSuccessParse(data: AnyObject) {
        
        if(GlobalVarible.Api == "driverlatlng"){
            
            if let driverlatlngdata = data as? DriverlatlngModel{
                
                self.driverlatlngdata = driverlatlngdata
                
                if(driverlatlngdata.result == 0){
                    
                    
                }else{
                    
                    self.timercallapiresponse()
                    
                }
                
                
                
            }
            
            
            
        }
        
        
        
        if(GlobalVarible.Api == "DriverInformation"){
            
            if let  mydatapage = data as? DriverInfo{
            
                self.mydatapage = mydatapage
            if(mydatapage.result == 1){
                
              //  k = 1
                
               
                
                self.redmarkerlocation.text = mydatapage.details!.dropLocation
                
                print(mydatapage.details!.dropLocation)
                
                self.greenmarkerlocation.text = mydatapage.details!.pickupLocation
                
                
                PickupLoc = (mydatapage.details!.pickupLocation)!
                
               currentStatus = (mydatapage.details?.rideStatus)!
                
            UserDefaults.standard.setValue(currentStatus, forKey:"firebaseride_status")
                
                 print(currentStatus)
                
                Driverdroplocation = mydatapage.details!.driverLocation!
                
                GlobalVarible.RideId = (mydatapage.details!.rideId)!
                GlobalVarible.driverphonenumber = (mydatapage.details!.driverPhone)!
                drivername.text = mydatapage.details!.driverName
                
                GlobalVarible.drivername = mydatapage.details!.driverName!
                
                carname.text = mydatapage.details!.carTypeName
                carmodelname.text = mydatapage.details!.carModelName
                
                carnumber.text = mydatapage.details!.carNumber
                
                let driverlat = Double((mydatapage.details!.driverLat)!)!
                let driverlng = Double((mydatapage.details!.driverLong)!)!
                
                
                DRIVERLAT  = String(format:"%f", driverlat)
                DRIVERLNG  = String(format:"%f", driverlng)
                
                driverid = (mydatapage.details?.driverId)!
                
               
                mapview.animate(toLocation: CLLocationCoordinate2D(latitude: Double(GlobalVarible.PickUpLat)!, longitude: Double(GlobalVarible.PickUpLng)!))
                mapview.animate(toZoom: 15)
                
                driverratingresult.text = mydatapage.details?.driverRating
                
                
                let driverratingvalue = mydatapage.details?.driverRating
                
                
                if driverratingvalue == ""{
                    print("hjjk")
                }else{
                    
                    // ratingview.rating = Float(driverratingvalue!)!
                    
                }
                
                let drivertypeimage = mydatapage.details!.driverImage
                
                print(drivertypeimage!)
                
                if(drivertypeimage == ""){
                    driverimageview.image = UIImage(named: "profileeee") as UIImage?
                    print("No Image")
                }else{
                    let newUrl = imageUrl + drivertypeimage!
                    
                    // let url = "http://apporio.co.uk/apporiotaxi/\(drivertypeimage!)"
                    // print(url)
                    
                    let url1 = NSURL(string: newUrl)
                    driverimageview!.af_setImage(withURL:
                        url1! as URL,
                                                  placeholderImage: UIImage(named: "dress"),
                                                  filter: nil,
                                                  imageTransition: .crossDissolve(1.0))
                }
                
                
                if(currentStatus == "3"){
                    
                    print(DRIVERLAT)
                    print(DRIVERLNG)
                    
                  //  let origin = GlobalVarible.PickUpLat + "," + GlobalVarible.PickUpLng
                 //   let destination = DRIVERLAT + "," + DRIVERLNG
                    
                 //   self.getDirections(origin: origin, destination: destination, waypoints: nil, travelMode: nil)
                      self.checkstarttimervalue = 1
                    self.starttimer()
                    ridestatustoplabel.text = "Arriving Now".localized
                    
                    sosbtn.isHidden = true
                    
                }
                
                if(currentStatus == "5"){
                    
                    k = 0
                    // self.Timer.invalidate()
                    
                    self.stopTimer()
                    cancelbutton.isHidden = true
                     cancelbuttonview.isHidden = true
                    sosbtn.isHidden = true
                    x = 1
                    startlat = mydatapage.details!.pickupLat!
                    startlng = mydatapage.details!.pickupLong!
                    
                    GlobalVarible.PickUpLat = mydatapage.details!.pickupLat!
                      
                    GlobalVarible.PickUpLng = mydatapage.details!.pickupLong!
                    
                    GlobalVarible.UserDropLat = Double(mydatapage.details!.dropLat!)!
                    
                    GlobalVarible.UserDropLng = Double((mydatapage.details?.dropLong!)!)!
                    
                    ridestatustoplabel.text = "Ride Arrived".localized
                    destinationlat = mydatapage.details!.dropLat!
                    destinationlng = mydatapage.details!.dropLong!
                    self.mapview.clear()
                    
                    self.drawRoute1()
                    
//                    let origin = startlat + "," + startlng
//                    let destination = destinationlat + "," + destinationlng
//
//                    self.getDirections(origin: origin, destination: destination, waypoints: nil, travelMode: nil)
                    
                 //   self.createRoute()
                    
                    // showTextToast(currentmessage)
                 //   self.showalert1(currentmessage)
                    
                    
                }
                
                if(currentStatus == "6"){
                    
                    k = 0
                    //  self.Timer.invalidate()
                  
                    
                    self.temparoryvalue = 1
                    
                    startlat = mydatapage.details!.pickupLat!
                    startlng = mydatapage.details!.pickupLong!
                    
                    GlobalVarible.PickUpLat = mydatapage.details!.pickupLat!
                    
                    GlobalVarible.PickUpLng = mydatapage.details!.pickupLong!
                    
                    GlobalVarible.UserDropLat =  Double(mydatapage.details!.dropLat!)!
                    GlobalVarible.UserDropLng = Double(mydatapage.details!.dropLong!)!

                    
                    destinationlat = String(GlobalVarible.UserDropLat)
                    destinationlng =  String(GlobalVarible.UserDropLng)
                    
                    
                    ridestatustoplabel.text = "Riding Now".localized
                    
                    Driverdroplocation = (mydatapage.details!.dropLocation)!
                    
                 //   self.showalert1(currentmessage)
                    
                    self.checkstarttimervalue = 2
                     self.starttimer()
                    
                    x = 1
                    cancelbutton.isHidden = true
                    cancelbuttonview.isHidden = true
                    sosbtn.isHidden = false
                    self.mapview.clear()
                    
                   
                    
                }
                  
                    
                   if(currentStatus == "7"){
                        
                    self.stopTimer()
                    
                    
                        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let nextController: PaymentWebViewController = storyboard.instantiateViewController(withIdentifier: "PaymentWebViewController") as! PaymentWebViewController
                        
                        nextController.currentrideid = mydatapage.details!.doneRideId!
                        
                        
                        self.present(nextController, animated: true, completion: nil)
                        
                        
                    }
                   
                   
                else{
                   
                    
                }
                
                
                
                
            }else{
                
                print("Hello")
                
                
            }
                
            }
            
        }

        if(GlobalVarible.Api == "Dropchange"){
            
            if let changedata = data as? DropChange{
                
                self.changedata = changedata
            
            if self.changedata.result == 1{
                
               /* self.redmarkerlocation.text = self.changedata.details?.dropLocation!
                
                destinationlat = (self.changedata.details?.dropLat!)!
                destinationlng = (self.changedata.details?.dropLong!)!
                
                
                let origin = startlat + "," + startlng
                let destination = destinationlat + "," + destinationlng
                
                self.getDirections(origin: origin, destination: destination, waypoints: nil, travelMode: nil)*/
                
                 self.dropchange = 0
                
                self.redmarkerlocation.text = changedata.details!.dropLocation
                
                self.greenmarkerlocation.text = changedata.details!.pickupLocation
                
                
                PickupLoc = (changedata.details!.pickupLocation)!
                
                 self.mapview.clear()
                Driverdroplocation = changedata.details!.driverLocation!
                
                GlobalVarible.RideId = (changedata.details!.rideId)!
                GlobalVarible.driverphonenumber = (changedata.details!.driverPhone)!
                drivername.text = changedata.details!.driverName
                
                carname.text = changedata.details!.carTypeName
                carmodelname.text = changedata.details!.carModelName
                
                carnumber.text = changedata.details!.carNumber
                
                let driverlat = Double((changedata.details!.driverLat)!)!
                let driverlng = Double((changedata.details!.driverLong)!)!
                
                
                DRIVERLAT  = String(format:"%f", driverlat)
                DRIVERLNG  = String(format:"%f", driverlng)
                
                driverid = (changedata.details?.driverId)!
                
                GlobalVarible.PickUpLat = changedata.details!.pickupLat!
                GlobalVarible.PickUpLng = (changedata.details?.pickupLong!)!
                
                GlobalVarible.UserDropLat = Double((changedata.details?.dropLat!)!)!
                
                GlobalVarible.UserDropLng = Double((changedata.details?.dropLong!)!)!
                
                
                mapview.animate(toLocation: CLLocationCoordinate2D(latitude: Double(GlobalVarible.PickUpLat)!, longitude: Double(GlobalVarible.PickUpLng)!))
                mapview.animate(toZoom: 15)
                
                driverratingresult.text = changedata.details?.driverRating
                
                
                let driverratingvalue = changedata.details?.driverRating
                
                
                if driverratingvalue == ""{
                    print("hjjk")
                }else{
                    
                    // ratingview.rating = Float(driverratingvalue!)!
                    
                }
                
                let drivertypeimage = changedata.details!.driverImage
                
                print(drivertypeimage!)
                
                if(drivertypeimage == ""){
                    driverimageview.image = UIImage(named: "profileeee") as UIImage?
                    print("No Image")
                }else{
                    let newUrl = imageUrl + drivertypeimage!
                    
                    // let url = "http://apporio.co.uk/apporiotaxi/\(drivertypeimage!)"
                    // print(url)
                    
                    let url1 = NSURL(string: newUrl)
                    driverimageview!.af_setImage(withURL:
                        url1! as URL,
                                                 placeholderImage: UIImage(named: "dress"),
                                                 filter: nil,
                                                 imageTransition: .crossDissolve(1.0))
                }
                
                
                if(currentStatus == "3"){
                    
                    print(DRIVERLAT)
                    print(DRIVERLNG)
                    
                    //  let origin = GlobalVarible.PickUpLat + "," + GlobalVarible.PickUpLng
                    //   let destination = DRIVERLAT + "," + DRIVERLNG
                    
                    //   self.getDirections(origin: origin, destination: destination, waypoints: nil, travelMode: nil)
                      self.checkstarttimervalue = 1
                    self.starttimer()
                    ridestatustoplabel.text = "Arriving Now".localized
                    sosbtn.isHidden = true
                    
                }
                
                if(currentStatus == "5"){
                    
                    k = 0
                    // self.Timer.invalidate()
                 
                    self.stopTimer()
                    // self.mapview.clear()
                    cancelbutton.isHidden = true
                    cancelbuttonview.isHidden = true
                    sosbtn.isHidden = true
                    x = 1
                    startlat = changedata.details!.pickupLat!
                    startlng = changedata.details!.pickupLong!
                    
                    ridestatustoplabel.text = "Ride Arrived".localized
                    destinationlat = changedata.details!.dropLat!
                    destinationlng = changedata.details!.dropLong!
                    self.drawRoute1()
                    
//                    let origin = startlat + "," + startlng
//                    let destination = destinationlat + "," + destinationlng
//
//                    self.getDirections(origin: origin, destination: destination, waypoints: nil, travelMode: nil)
                    
                    //   self.createRoute()
                    
                    // showTextToast(currentmessage)
                    //   self.showalert1(currentmessage)
                    
                    
                }
                
                if(currentStatus == "6"){
                    
                    k = 0
                    //  self.Timer.invalidate()
                  
                    
                    self.temparoryvalue = 1
                    
                    startlat = changedata.details!.pickupLat!
                    startlng = changedata.details!.pickupLong!
                    
                    GlobalVarible.UserDropLat =  Double(changedata.details!.dropLat!)!
                    GlobalVarible.UserDropLng = Double(changedata.details!.dropLong!)!
                    
                    
                    destinationlat = String(GlobalVarible.UserDropLat)
                    destinationlng =  String(GlobalVarible.UserDropLng)
                    
                    
                    ridestatustoplabel.text = "Riding Now".localized
                    
                    Driverdroplocation = (changedata.details!.dropLocation)!
                    
                    //   self.showalert1(currentmessage)
                    self.drawRoute1()
//                    let origin = startlat + "," + startlng
//                    let destination = destinationlat + "," + destinationlng
//
//                    self.getDirections(origin: origin, destination: destination, waypoints: nil, travelMode: nil)
                    self.checkstarttimervalue = 2
                    self.starttimer()
                    
                    x = 1
                    cancelbutton.isHidden = true
                    cancelbuttonview.isHidden = true
                    sosbtn.isHidden = false
                    self.mapview.clear()
                    
                   
                    
                }
                else{
                   
                    
                }
                
                
                }
                
            }
        }
        
        if(GlobalVarible.Api == "rentalshareurl"){
            
            
            if let ridesharedata = data as? RideShareModel{
            
                self.ridesharedata = ridesharedata
                
            if self.ridesharedata.result == "1" {
                
               /* let shareText = "http://www.apporiotaxi.com/Apporiotaxi/Ride/Track/" + ridesharedata.details!
                let activityViewController = UIActivityViewController(activityItems: [shareText], applicationActivities: nil)
                present(activityViewController, animated: true, completion: nil)*/
                
                
                let shareText = API_URL.sharelinkurl + "Ride/Track/" + ridesharedata.details!
                
                print(shareText)
                
                let activityViewController = UIActivityViewController(activityItems: [shareText], applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view
                activityViewController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection()
                activityViewController.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
                
                self.present(activityViewController, animated: true) {
                    print("option menu presented")
                }
                

            
            
            
            }else{
            
            self.showalert(message: ridesharedata.message!)
            }
        
        
            }
        
        }
        
        
        
    }
    
 
   
}


extension TrackRideViewController {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print(place)
        print("Place name: \(place.name)")
        print("Place address: \(place.formattedAddress)")
        print("Place attributions: \(place.attributions)")
        print("Place attributions: \(place.coordinate.latitude)")
        print("Place attributions: \(place.coordinate.longitude)")
        //  manualloactioncheck = "manual"
        
        // GlobalVarible.Pickuptext = place.formattedAddress!
        // GlobalVarible.UserDropLocationText = place.formattedAddress!
        GlobalVarible.UserDropLocationText = place.name
        GlobalVarible.UserDropLat = place.coordinate.latitude
        GlobalVarible.UserDropLng = place.coordinate.longitude
        
        self.dropchange = 1
        
        
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}

