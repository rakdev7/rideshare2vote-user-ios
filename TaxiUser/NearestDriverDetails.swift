//
//  Details.swift
//
//  Created by Atul Jain on 11/05/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public class NearestDriverDetails {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let driverId = "driver_id"
    static let currentLat = "current_lat"
    static let bearingfactor = "bearing_factor"
    static let currentLong = "current_long"
    static let distance = "distance"
  }

  // MARK: Properties
  public var driverId: String?
  public var currentLat: String?
  public var bearingfactor: String?
  public var currentLong: String?
  public var distance: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    driverId = json[SerializationKeys.driverId].string
    currentLat = json[SerializationKeys.currentLat].string
    bearingfactor = json[SerializationKeys.bearingfactor].string
    currentLong = json[SerializationKeys.currentLong].string
    distance = json[SerializationKeys.distance].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = driverId { dictionary[SerializationKeys.driverId] = value }
    if let value = currentLat { dictionary[SerializationKeys.currentLat] = value }
    if let value = bearingfactor { dictionary[SerializationKeys.bearingfactor] = value }
    if let value = currentLong { dictionary[SerializationKeys.currentLong] = value }
    if let value = distance { dictionary[SerializationKeys.distance] = value }
    return dictionary
  }

}
