//
//  Refer&earnViewController.swift
//  Apporio Taxi
//
//  Created by Atul Jain on 22/03/18.
//  Copyright © 2018 apporio. All rights reserved.
//

import UIKit

class Refer_earnViewController: UIViewController,MainCategoryProtocol {
    
    
    @IBOutlet weak var tellyourfriendtextlbl: UILabel!
    @IBOutlet weak var referearntextlbl: UILabel!
    var invitecodedata : InviteCodeModel!

    @IBOutlet weak var scrollview: UIScrollView!
    
    @IBOutlet weak var sharecodebtntext: UIButton!
    
    @IBOutlet weak var invitecodetext: UILabel!
    @IBOutlet weak var offertypetext: UILabel!
    @IBOutlet weak var enddatetext: UILabel!
    @IBOutlet weak var startdatetext: UILabel!
    
    var code = ""
    
    
    @IBOutlet weak var inviteyourfriendstextlbl: UILabel!
    var ioslink = ""
    @IBOutlet weak var offertypetextlbl: UILabel!
    
    @IBOutlet weak var enddatetextlbl: UILabel!
    var androidlink = ""
    
    @IBOutlet weak var startdatetextlbl: UILabel!
    @IBOutlet weak var promocodedetailstextlbl: UILabel!
    let Userid = NsUserDekfaultManager.SingeltionInstance.getuserdetaild(key: NsUserDekfaultManager.Keyuserid)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setColor()
        tellyourfriendtextlbl.text = "Tell your friends about RideShare2Vote Taxi".localized
       // referearntextlbl.text = "Refer & Earn".localized
        referearntextlbl.text = "Refer".localized
         inviteyourfriendstextlbl.text = "Invite your friends".localized
        sharecodebtntext.setTitle("Share With Friends".localized, for: UIControlState.normal)
//        inviteyourfriendstextlbl.text = "Invite your friends using your promo code and you both earn.".localized
        promocodedetailstextlbl.text = "Promo Code Details :".localized
        startdatetextlbl.text = "Start Date :".localized
        enddatetextlbl.text = "End Date :".localized
        offertypetextlbl.text = "Offer Type :".localized
//        sharecodebtntext.setTitle("Share Your Code".localized, for: UIControlState.normal)
        
        sharecodebtntext.layer.shadowColor = UIColor.gray.cgColor
       sharecodebtntext.layer.shadowOpacity = 1
        sharecodebtntext.layer.shadowOffset = CGSize(width: 0, height: 2)
        sharecodebtntext.layer.shadowRadius = 2

        
        let borderLayer  = dashedBorderLayerWithColor(color: UIColor.red.cgColor)
        
        self.invitecodetext.layer.addSublayer(borderLayer)
        
        ApiManager.sharedInstance.protocolmain_Catagory = self
        ApiManager.sharedInstance.InviteCodeMethod(UserId: self.Userid!)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillLayoutSubviews() {
        
        
        
        super.viewWillLayoutSubviews()
        self.scrollview.frame = self.scrollview.bounds
        self.scrollview.contentSize.height =  700
        self.scrollview.contentSize.width = 0
    }
    
    func setColor() {
       sharecodebtntext.backgroundColor = AppColors.themeColor
    }
    
    func dashedBorderLayerWithColor(color:CGColor) -> CAShapeLayer {
        
        let  borderLayer = CAShapeLayer()
        borderLayer.name  = "borderLayer"
        let frameSize = invitecodetext.frame.size
        
        let shapeRect = CGRect(x: 0.0, y: 0.0, width: frameSize.width, height: frameSize.height)
        
        borderLayer.bounds=shapeRect
        borderLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
        
        borderLayer.fillColor = UIColor.clear.cgColor
        borderLayer.strokeColor = color
        borderLayer.lineWidth=1.5
        borderLayer.lineJoin=kCALineJoinRound
        borderLayer.lineDashPattern = NSArray(array: [NSNumber(value: 8),NSNumber(value:4)]) as? [NSNumber]
        
        let path = UIBezierPath.init(roundedRect: shapeRect, cornerRadius: 0)
        
        borderLayer.path = path.cgPath
        
        return borderLayer
        
    }
    
    
    @IBAction func backbtnclick(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func sharecodebtnclick(_ sender: Any) {
        
        let shareText = "Haven't tried Apporio Taxi yet? Sign up with my code " + "(" +  code + ")" + " and enjoy the most affordable can rides! \n"  +  "Download app from below link \n" +  ioslink         
        print(shareText)
        
        let activityViewController = UIActivityViewController(activityItems: [shareText], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        activityViewController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection()
        activityViewController.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        
        self.present(activityViewController, animated: true) {
            print("option menu presented")
        }
        
        

    }
    
    
    func showalert(message:String)  {
        
        DispatchQueue.main.async(execute: {
            
            let alertController = UIAlertController(title:   "Alert".localized, message:message, preferredStyle: .alert)
            
            
            let OKAction = UIAlertAction(title: "ok".localized, style: .default) { (action) in
                
            }
            alertController.addAction(OKAction)
            
            self.present(alertController, animated: true) {
                
            }
            
            
        })
        
    }
    

    
    
    func onProgressStatus(value: Int) {
        if(value == 0 ){
            MBProgressHUD.hide(for: self.view, animated: true)
        }else if (value == 1){
            let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
            spinnerActivity.label.text = "Loading".localized
            spinnerActivity.detailsLabel.text = "Please Wait!!".localized
            spinnerActivity.isUserInteractionEnabled = false
            
        }
    }
    
    func onSuccessExecution(msg: String) {
        print("\(msg)")
    }
    
    
    func onerror(msg : String) {
        MBProgressHUD.hide(for: self.view, animated: true)
        
        self.showalert(message: msg)
        
        
    }
    
    
    func onSuccessParse(data: AnyObject) {
        
        if(GlobalVarible.Api == "invitecode"){
            
            if let  invitecodedata = data as? InviteCodeModel{
            
                self.invitecodedata = invitecodedata
            if invitecodedata.result == 1{
                
                startdatetext.text = invitecodedata.details?.startDate
                
                enddatetext.text = invitecodedata.details?.endDate
                
                offertypetext.text = invitecodedata.details?.offer
                
                invitecodetext.text = invitecodedata.details?.referralCode
                
                code = (invitecodedata.details?.referralCode)!
                
                ioslink = (invitecodedata.details?.applicationurl)!
            
            
            }else{
            
             invitecodetext.text = invitecodedata.msg
            }

            }
        }
        
        
    }
    

}
