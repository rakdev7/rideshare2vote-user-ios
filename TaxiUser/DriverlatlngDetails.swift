//
//  Details.swift
//
//  Created by Atul Jain on 09/05/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public class DriverlatlngDetails {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let verificationStatus = "verification_status"
    static let insuranceExpire = "insurance_expire"
    static let driverName = "driver_name"
    static let driverToken = "driver_token"
    static let driverPayment = "driver_payment"
    static let driverBankName = "driver_bank_name"
    static let busy = "busy"
    static let totalDocumentNeed = "total_document_need"
    static let driverPassword = "driver_password"
    static let bearingFactor = "bearing_factor"
    static let verfiyDocument = "verfiy_document"
    static let otherDocs = "other_docs"
    static let completedRides = "completed_rides"
    static let carNumber = "car_number"
    static let companyPayment = "company_payment"
    static let commission = "commission"
    static let carModelId = "car_model_id"
    static let detailStatus = "detail_status"
    static let driverId = "driver_id"
    static let rating = "rating"
    static let license = "license"
    static let insurance = "insurance"
    static let loginLogout = "login_logout"
    static let carTypeId = "car_type_id"
    static let driverPhone = "driver_phone"
    static let onlineOffline = "online_offline"
    static let flag = "flag"
    static let nidPpNo = "nid_pp_no"
    static let rc = "rc"
    static let companyId = "company_id"
    static let verificationDate = "verification_date"
    static let rcExpire = "rc_expire"
    static let currentLat = "current_lat"
    static let uniqueNumber = "unique_number"
    static let totalCardPayment = "total_card_payment"
    static let amountTransferPending = "amount_transfer_pending"
    static let driverStatusMessage = "driver_status_message"
    static let driverCategory = "driver_category"
    static let lastUpdateDate = "last_update_date"
    static let driverAccountName = "driver_account_name"
    static let licenseExpire = "license_expire"
    static let driverEmail = "driver_email"
    static let paymentTransfer = "payment_transfer"
    static let lastUpdate = "last_update"
    static let driverAccountNumber = "driver_account_number"
    static let totalCashPayment = "total_cash_payment"
    static let driverDelete = "driver_delete"
    static let currentLong = "current_long"
    static let currentLocation = "current_location"
    static let cityId = "city_id"
    static let cancelledRides = "cancelled_rides"
    static let driverAdminStatus = "driver_admin_status"
    static let gender = "gender"
    static let driverStatusImage = "driver_status_image"
    static let rejectRides = "reject_rides"
    static let driverSignupDate = "driver_signup_date"
    static let totalPaymentEraned = "total_payment_eraned"
    static let deviceId = "device_id"
    static let driverImage = "driver_image"
    static let registerDate = "register_date"
  }

  // MARK: Properties
  public var verificationStatus: String?
  public var insuranceExpire: String?
  public var driverName: String?
  public var driverToken: String?
  public var driverPayment: String?
  public var driverBankName: String?
  public var busy: String?
  public var totalDocumentNeed: String?
  public var driverPassword: String?
  public var bearingFactor: String?
  public var verfiyDocument: String?
  public var otherDocs: String?
  public var completedRides: String?
  public var carNumber: String?
  public var companyPayment: String?
  public var commission: String?
  public var carModelId: String?
  public var detailStatus: String?
  public var driverId: String?
  public var rating: String?
  public var license: String?
  public var insurance: String?
  public var loginLogout: String?
  public var carTypeId: String?
  public var driverPhone: String?
  public var onlineOffline: String?
  public var flag: String?
  public var nidPpNo: String?
  public var rc: String?
  public var companyId: String?
  public var verificationDate: String?
  public var rcExpire: String?
  public var currentLat: String?
  public var uniqueNumber: String?
  public var totalCardPayment: String?
  public var amountTransferPending: String?
  public var driverStatusMessage: String?
  public var driverCategory: String?
  public var lastUpdateDate: String?
  public var driverAccountName: String?
  public var licenseExpire: String?
  public var driverEmail: String?
  public var paymentTransfer: String?
  public var lastUpdate: String?
  public var driverAccountNumber: String?
  public var totalCashPayment: String?
  public var driverDelete: String?
  public var currentLong: String?
  public var currentLocation: String?
  public var cityId: String?
  public var cancelledRides: String?
  public var driverAdminStatus: String?
  public var gender: String?
  public var driverStatusImage: String?
  public var rejectRides: String?
  public var driverSignupDate: String?
  public var totalPaymentEraned: String?
  public var deviceId: String?
  public var driverImage: String?
  public var registerDate: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    verificationStatus = json[SerializationKeys.verificationStatus].string
    insuranceExpire = json[SerializationKeys.insuranceExpire].string
    driverName = json[SerializationKeys.driverName].string
    driverToken = json[SerializationKeys.driverToken].string
    driverPayment = json[SerializationKeys.driverPayment].string
    driverBankName = json[SerializationKeys.driverBankName].string
    busy = json[SerializationKeys.busy].string
    totalDocumentNeed = json[SerializationKeys.totalDocumentNeed].string
    driverPassword = json[SerializationKeys.driverPassword].string
    bearingFactor = json[SerializationKeys.bearingFactor].string
    verfiyDocument = json[SerializationKeys.verfiyDocument].string
    otherDocs = json[SerializationKeys.otherDocs].string
    completedRides = json[SerializationKeys.completedRides].string
    carNumber = json[SerializationKeys.carNumber].string
    companyPayment = json[SerializationKeys.companyPayment].string
    commission = json[SerializationKeys.commission].string
    carModelId = json[SerializationKeys.carModelId].string
    detailStatus = json[SerializationKeys.detailStatus].string
    driverId = json[SerializationKeys.driverId].string
    rating = json[SerializationKeys.rating].string
    license = json[SerializationKeys.license].string
    insurance = json[SerializationKeys.insurance].string
    loginLogout = json[SerializationKeys.loginLogout].string
    carTypeId = json[SerializationKeys.carTypeId].string
    driverPhone = json[SerializationKeys.driverPhone].string
    onlineOffline = json[SerializationKeys.onlineOffline].string
    flag = json[SerializationKeys.flag].string
    nidPpNo = json[SerializationKeys.nidPpNo].string
    rc = json[SerializationKeys.rc].string
    companyId = json[SerializationKeys.companyId].string
    verificationDate = json[SerializationKeys.verificationDate].string
    rcExpire = json[SerializationKeys.rcExpire].string
    currentLat = json[SerializationKeys.currentLat].string
    uniqueNumber = json[SerializationKeys.uniqueNumber].string
    totalCardPayment = json[SerializationKeys.totalCardPayment].string
    amountTransferPending = json[SerializationKeys.amountTransferPending].string
    driverStatusMessage = json[SerializationKeys.driverStatusMessage].string
    driverCategory = json[SerializationKeys.driverCategory].string
    lastUpdateDate = json[SerializationKeys.lastUpdateDate].string
    driverAccountName = json[SerializationKeys.driverAccountName].string
    licenseExpire = json[SerializationKeys.licenseExpire].string
    driverEmail = json[SerializationKeys.driverEmail].string
    paymentTransfer = json[SerializationKeys.paymentTransfer].string
    lastUpdate = json[SerializationKeys.lastUpdate].string
    driverAccountNumber = json[SerializationKeys.driverAccountNumber].string
    totalCashPayment = json[SerializationKeys.totalCashPayment].string
    driverDelete = json[SerializationKeys.driverDelete].string
    currentLong = json[SerializationKeys.currentLong].string
    currentLocation = json[SerializationKeys.currentLocation].string
    cityId = json[SerializationKeys.cityId].string
    cancelledRides = json[SerializationKeys.cancelledRides].string
    driverAdminStatus = json[SerializationKeys.driverAdminStatus].string
    gender = json[SerializationKeys.gender].string
    driverStatusImage = json[SerializationKeys.driverStatusImage].string
    rejectRides = json[SerializationKeys.rejectRides].string
    driverSignupDate = json[SerializationKeys.driverSignupDate].string
    totalPaymentEraned = json[SerializationKeys.totalPaymentEraned].string
    deviceId = json[SerializationKeys.deviceId].string
    driverImage = json[SerializationKeys.driverImage].string
    registerDate = json[SerializationKeys.registerDate].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = verificationStatus { dictionary[SerializationKeys.verificationStatus] = value }
    if let value = insuranceExpire { dictionary[SerializationKeys.insuranceExpire] = value }
    if let value = driverName { dictionary[SerializationKeys.driverName] = value }
    if let value = driverToken { dictionary[SerializationKeys.driverToken] = value }
    if let value = driverPayment { dictionary[SerializationKeys.driverPayment] = value }
    if let value = driverBankName { dictionary[SerializationKeys.driverBankName] = value }
    if let value = busy { dictionary[SerializationKeys.busy] = value }
    if let value = totalDocumentNeed { dictionary[SerializationKeys.totalDocumentNeed] = value }
    if let value = driverPassword { dictionary[SerializationKeys.driverPassword] = value }
    if let value = bearingFactor { dictionary[SerializationKeys.bearingFactor] = value }
    if let value = verfiyDocument { dictionary[SerializationKeys.verfiyDocument] = value }
    if let value = otherDocs { dictionary[SerializationKeys.otherDocs] = value }
    if let value = completedRides { dictionary[SerializationKeys.completedRides] = value }
    if let value = carNumber { dictionary[SerializationKeys.carNumber] = value }
    if let value = companyPayment { dictionary[SerializationKeys.companyPayment] = value }
    if let value = commission { dictionary[SerializationKeys.commission] = value }
    if let value = carModelId { dictionary[SerializationKeys.carModelId] = value }
    if let value = detailStatus { dictionary[SerializationKeys.detailStatus] = value }
    if let value = driverId { dictionary[SerializationKeys.driverId] = value }
    if let value = rating { dictionary[SerializationKeys.rating] = value }
    if let value = license { dictionary[SerializationKeys.license] = value }
    if let value = insurance { dictionary[SerializationKeys.insurance] = value }
    if let value = loginLogout { dictionary[SerializationKeys.loginLogout] = value }
    if let value = carTypeId { dictionary[SerializationKeys.carTypeId] = value }
    if let value = driverPhone { dictionary[SerializationKeys.driverPhone] = value }
    if let value = onlineOffline { dictionary[SerializationKeys.onlineOffline] = value }
    if let value = flag { dictionary[SerializationKeys.flag] = value }
    if let value = nidPpNo { dictionary[SerializationKeys.nidPpNo] = value }
    if let value = rc { dictionary[SerializationKeys.rc] = value }
    if let value = companyId { dictionary[SerializationKeys.companyId] = value }
    if let value = verificationDate { dictionary[SerializationKeys.verificationDate] = value }
    if let value = rcExpire { dictionary[SerializationKeys.rcExpire] = value }
    if let value = currentLat { dictionary[SerializationKeys.currentLat] = value }
    if let value = uniqueNumber { dictionary[SerializationKeys.uniqueNumber] = value }
    if let value = totalCardPayment { dictionary[SerializationKeys.totalCardPayment] = value }
    if let value = amountTransferPending { dictionary[SerializationKeys.amountTransferPending] = value }
    if let value = driverStatusMessage { dictionary[SerializationKeys.driverStatusMessage] = value }
    if let value = driverCategory { dictionary[SerializationKeys.driverCategory] = value }
    if let value = lastUpdateDate { dictionary[SerializationKeys.lastUpdateDate] = value }
    if let value = driverAccountName { dictionary[SerializationKeys.driverAccountName] = value }
    if let value = licenseExpire { dictionary[SerializationKeys.licenseExpire] = value }
    if let value = driverEmail { dictionary[SerializationKeys.driverEmail] = value }
    if let value = paymentTransfer { dictionary[SerializationKeys.paymentTransfer] = value }
    if let value = lastUpdate { dictionary[SerializationKeys.lastUpdate] = value }
    if let value = driverAccountNumber { dictionary[SerializationKeys.driverAccountNumber] = value }
    if let value = totalCashPayment { dictionary[SerializationKeys.totalCashPayment] = value }
    if let value = driverDelete { dictionary[SerializationKeys.driverDelete] = value }
    if let value = currentLong { dictionary[SerializationKeys.currentLong] = value }
    if let value = currentLocation { dictionary[SerializationKeys.currentLocation] = value }
    if let value = cityId { dictionary[SerializationKeys.cityId] = value }
    if let value = cancelledRides { dictionary[SerializationKeys.cancelledRides] = value }
    if let value = driverAdminStatus { dictionary[SerializationKeys.driverAdminStatus] = value }
    if let value = gender { dictionary[SerializationKeys.gender] = value }
    if let value = driverStatusImage { dictionary[SerializationKeys.driverStatusImage] = value }
    if let value = rejectRides { dictionary[SerializationKeys.rejectRides] = value }
    if let value = driverSignupDate { dictionary[SerializationKeys.driverSignupDate] = value }
    if let value = totalPaymentEraned { dictionary[SerializationKeys.totalPaymentEraned] = value }
    if let value = deviceId { dictionary[SerializationKeys.deviceId] = value }
    if let value = driverImage { dictionary[SerializationKeys.driverImage] = value }
    if let value = registerDate { dictionary[SerializationKeys.registerDate] = value }
    return dictionary
  }

}
